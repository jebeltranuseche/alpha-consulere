CREATE DATABASE  `alpha_consulere` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE alpha_consulere;
-- MySQL dump 10.13  Distrib 5.7.20, for Linux (x86_64)
--
-- Host: localhost    Database: alpha_consulere
-- ------------------------------------------------------
-- Server version	5.7.28-0ubuntu0.16.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `contract_tiers`
--
DROP TABLE IF EXISTS users;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE SEQUENCE users_seq;

CREATE TABLE users (
  id bigint NOT NULL DEFAULT NEXTVAL ('users_seq'),
  created_at timestamp(0) NOT NULL,
  updated_at timestamp(0) NOT NULL,
  email varchar(40) DEFAULT NULL,
  password varchar(100) DEFAULT NULL,
  username varchar(15) DEFAULT NULL,
  name varchar(40) DEFAULT NULL,
  created_by bigint DEFAULT NULL,
  updated_by bigint DEFAULT NULL,
  owner_id bigint DEFAULT NULL,
  tier_id bigint DEFAULT NULL,
  PRIMARY KEY (id),
  CONSTRAINT UKr43af9ap4edm43mmtq01oddj6 UNIQUE  (username),
  CONSTRAINT UK6dotkott2kjsp8vw4d0m25fb7 UNIQUE  (email)
 ,
  CONSTRAINT FKgpa48vmq0y8krwjtlfjh4tean FOREIGN KEY (tier_id) REFERENCES tiers (id),
  CONSTRAINT FKntyuh06i5y3y6ir598luxy3k9 FOREIGN KEY (owner_id) REFERENCES users (id)
)   ;

ALTER SEQUENCE users_seq RESTART WITH 23;
/*!40101 SET character_set_client = @saved_cs_client */;

CREATE INDEX FKntyuh06i5y3y6ir598luxy3k9 ON users (owner_id);
CREATE INDEX FKgpa48vmq0y8krwjtlfjh4tean ON users (tier_id);

--
-- Dumping data for table `users`
--


/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO users VALUES (1,'2019-09-22 15:54:52','2019-09-22 15:54:52','jebeltranuseche@gmail.com','$2a$10$KpWDCFgi/x8CrGDDEclJxOfSYTM2uH5tmIEIdaKraGYoTjfNKrQcK','jebmoon',NULL,NULL,NULL,NULL,NULL),(2,'2019-11-06 21:39:30','2019-11-06 21:39:30','loco@loco.com','$2a$10$9ySVZADcCOSK77Pqt2XQk..o2Vv8bwqkJH5RG2aedIMTpaMkUrcdW','afrotsukoyomi',NULL,NULL,NULL,NULL,NULL),(3,'2019-11-07 20:04:18','2019-11-07 20:04:18','re@re.com','$2a$10$Ps29Ud1keS5EBhphWmcDY.3NqGYVMY/5rvzzDG1fVLqKZoAcPxo8q','epa',NULL,1,1,NULL,NULL),(4,'2019-11-07 23:18:41','2019-11-07 23:18:41','re1@re.com','$2a$10$Umb8/bfmOeKvoh3jgqvnpedyopNE7DJackdaQoG8G/nc5Xj7ukdtu','prueba',NULL,1,1,1,NULL),(5,'2019-11-24 15:09:44','2019-11-24 15:09:44','mike@re.com','$2a$10$pRjuDJUZprREkbqb/5ey9.oSyjFoUd0Tb630GZ9L/Tjwr8CMPIG2i','elcrack',NULL,1,1,1,1),(6,'2019-11-24 15:12:19','2019-11-24 15:12:19','filmalove@re.com','$2a$10$YiI1gGahXHN4woXkF8FeNOVGMlzMeww6ulOgbOBE7y6jWMHqOmp/W','doyuwana',NULL,1,1,1,NULL),(15,'2019-11-24 19:59:07','2019-11-24 19:59:07','sujeto1@re.com','$2a$10$m.ovlgoQuPFaxXn9QpZAm.na2MzDv9V/m3jRGre.xYOobAO0p1tEO','sujeto1',NULL,5,5,5,NULL),(16,'2019-11-24 19:59:16','2019-11-24 19:59:16','sujeto2@re.com','$2a$10$uCbsDffVveVy8qWk2Vyec.IqAVMuEdvM7trwodKDMtyZKIfJrFRYO','sujeto2',NULL,5,5,5,NULL),(17,'2019-11-24 20:00:42','2019-11-24 20:00:42','sujeto3@re.com','$2a$10$O1KLJRdKT0Nbu.vXu6Futec5eoCIR.MsXGIZp3CUvyPPpRmq7I4jm','sujeto3',NULL,5,5,5,NULL),(18,'2019-11-24 21:32:47','2019-11-24 21:32:47','sujetoa@re.com','$2a$10$zt68JxXU.w4J9hSqHsLZLeHGdYC0NflCyYTlNNzZODH6YiBAZIrbu','sujeto',NULL,1,1,1,NULL),(19,'2019-11-24 21:33:07','2019-11-24 21:33:07','xd@re.com','$2a$10$ExQri1hKzOcrC5V20Q2uMOFP0Mmay3c7PxoixaTL9RZ57bOUIzecS','xsasdasd',NULL,1,1,1,NULL),(20,'2019-11-25 16:42:54','2019-11-25 16:42:54','xds@re.com','$2a$10$GKUQXV2ITss.XjD3hw5ku.WEOpR8a/0EtR5JotCeT/SLICq/n.bp.','xssasdasd',NULL,1,1,1,NULL),(21,'2019-12-01 18:27:05','2019-12-01 18:27:05','xd22@re.com','$2a$10$gmLsS2iiiaVTulPAu/GtVuQRqd/y/8I3Qgc0gAGu8T4Gbzx/fLEve','xssass2dasd',NULL,5,5,5,NULL),(22,'2019-12-01 18:31:46','2019-12-01 18:31:46','xd212@re.com','$2a$10$WgT1/0yIJeTBsFzclcqoM.qOeek.qj7f62ejnA2dh22scrgybXkSa','xssass222dasd',NULL,5,5,5,NULL);


DROP TABLE IF EXISTS roles;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE SEQUENCE roles_seq;

CREATE TABLE roles (
  id bigint NOT NULL DEFAULT NEXTVAL ('roles_seq'),
  created_at timestamp(0) NOT NULL,
  created_by bigint DEFAULT NULL,
  updated_at timestamp(0) NOT NULL,
  updated_by bigint DEFAULT NULL,
  description varchar(256) DEFAULT NULL,
  name varchar(15) DEFAULT NULL,
  PRIMARY KEY (id)
)   ;

ALTER SEQUENCE roles_seq RESTART WITH 4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO roles VALUES (1,'2019-11-06 21:35:51',0,'2019-11-06 21:35:51',0,'ROLE_USER','ROLE_USER'),(2,'2019-11-07 23:16:20',0,'2019-11-07 23:16:20',0,'ADMIN','ROLE_ADMIN'),(3,'2019-11-24 16:02:51',0,'2019-11-24 16:02:51',0,'CLIENTE DE ALPHA CONSULERE','ROLE_CLIENT');



DROP TABLE IF EXISTS roles_users;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE roles_users (
  user_id bigint NOT NULL,
  role_id bigint NOT NULL,
  PRIMARY KEY (user_id,role_id)
 ,
  CONSTRAINT FKlkcn1l0gnfshcn4rnmak73ta FOREIGN KEY (user_id) REFERENCES users (id),
  CONSTRAINT FKsmos02hm32191ogexm2ljik9x FOREIGN KEY (role_id) REFERENCES roles (id)
)  ;

CREATE INDEX FKsmos02hm32191ogexm2ljik9x ON roles_users (role_id);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles_users`
--

/*!40000 ALTER TABLE `roles_users` DISABLE KEYS */;
INSERT INTO roles_users VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1),(22,1),(1,2),(5,2),(5,3);


--
-- Table structure for table `permissions`
--


CREATE TABLE permissions (
  id bigint NOT NULL DEFAULT NEXTVAL ('permissions_seq'),
  created_at timestamp(0) NOT NULL,
  created_by bigint DEFAULT NULL,
  updated_at timestamp(0) NOT NULL,
  updated_by bigint DEFAULT NULL,
  description varchar(256) DEFAULT NULL,
  name varchar(15) DEFAULT NULL,
  PRIMARY KEY (id)
)  ;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--
-- Table structure for table `permissions_roles`
--

DROP TABLE IF EXISTS permissions_roles;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE permissions_roles (
  role_id bigint NOT NULL,
  permission_id bigint NOT NULL,
  PRIMARY KEY (role_id,permission_id)
 ,
  CONSTRAINT FK9j7vx1vojmoa6rs21eggd46xn FOREIGN KEY (role_id) REFERENCES roles (id),
  CONSTRAINT FKff6bcp6bbaup2irutar3dfaks FOREIGN KEY (permission_id) REFERENCES permissions (id)
)  ;

CREATE INDEX FKff6bcp6bbaup2irutar3dfaks ON permissions_roles (permission_id);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions_roles`
--



--
-- Table structure for table `roles`
--



--
-- Table structure for table `roles_users`
--

--
-- Table structure for table `tiers`
--

DROP TABLE IF EXISTS tiers;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE SEQUENCE tiers_seq;

CREATE TABLE tiers (
  id bigint NOT NULL DEFAULT NEXTVAL ('tiers_seq'),
  created_at timestamp(0) NOT NULL,
  created_by bigint NOT NULL,
  updated_at timestamp(0) NOT NULL,
  updated_by bigint NOT NULL,
  name varchar(15) DEFAULT NULL,
  PRIMARY KEY (id),
  CONSTRAINT UKj8mu6i3mlyw79w9xewnha31kt UNIQUE  (name)
)   ;

ALTER SEQUENCE tiers_seq RESTART WITH 2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tiers`
--


/*!40000 ALTER TABLE `tiers` DISABLE KEYS */;
INSERT INTO tiers VALUES (1,'2019-11-24 16:05:07',0,'2019-11-24 16:05:07',0,'bronce');
/*!40000 ALTER TABLE `tiers` ENABLE KEYS */;


DROP TABLE IF EXISTS contract_tiers;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE SEQUENCE contract_tiers_seq;

CREATE TABLE contract_tiers (
  id bigint NOT NULL DEFAULT NEXTVAL ('contract_tiers_seq'),
  created_at timestamp(0) NOT NULL,
  created_by bigint NOT NULL,
  updated_at timestamp(0) NOT NULL,
  updated_by bigint NOT NULL,
  end_date timestamp(0) DEFAULT NULL,
  affiliated_id bigint DEFAULT NULL,
  tier_id bigint DEFAULT NULL,
  PRIMARY KEY (id),
  CONSTRAINT UKmnj9ljq1ghukdmue1omntgulp UNIQUE  (affiliated_id,tier_id)
 ,
  CONSTRAINT FK172u7gp7mynlxj7ifg6d5u0ku FOREIGN KEY (affiliated_id) REFERENCES users (id),
  CONSTRAINT FKtpnbiq6xpeebo5sbr018cb08p FOREIGN KEY (tier_id) REFERENCES tiers (id)
)   ;

ALTER SEQUENCE contract_tiers_seq RESTART WITH 5;
/*!40101 SET character_set_client = @saved_cs_client */;

CREATE INDEX FKtpnbiq6xpeebo5sbr018cb08p ON contract_tiers (tier_id);

--
-- Dumping data for table `contract_tiers`
--


/*!40000 ALTER TABLE `contract_tiers` DISABLE KEYS */;
INSERT INTO contract_tiers VALUES (3,'2019-12-01 18:20:11',1,'2019-12-01 18:20:11',1,'2019-12-05 20:00:00',5,1),(4,'2019-12-04 13:51:31',5,'2019-12-04 13:51:31',5,'2019-12-05 20:00:00',6,1);
/*!40000 ALTER TABLE `contract_tiers` ENABLE KEYS */;


--
-- Table structure for table `creation_capacities`
--

DROP TABLE IF EXISTS creation_capacities;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE SEQUENCE creation_capacities_seq;

CREATE TABLE creation_capacities (
  id bigint NOT NULL DEFAULT NEXTVAL ('creation_capacities_seq'),
  created_at timestamp(0) NOT NULL,
  created_by bigint NOT NULL,
  updated_at timestamp(0) NOT NULL,
  updated_by bigint NOT NULL,
  capacity int NOT NULL,
  entity varchar(15) DEFAULT NULL,
  tier_id bigint DEFAULT NULL,
  PRIMARY KEY (id),
  CONSTRAINT UKkl41ilrrp5arxa9v0lt2iesl UNIQUE  (tier_id,entity),
  CONSTRAINT FKiurbsrpyofxtvxgojxyamkow0 FOREIGN KEY (tier_id) REFERENCES tiers (id)
)   ;

ALTER SEQUENCE creation_capacities_seq RESTART WITH 3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `creation_capacities`
--


/*!40000 ALTER TABLE `creation_capacities` DISABLE KEYS */;
INSERT INTO creation_capacities VALUES (1,'2019-11-24 16:14:12',0,'2019-11-24 16:14:12',0,5,'users',1),(2,'2019-11-24 16:14:13',0,'2019-11-24 16:14:13',0,1,'companies',1);




--
-- Table structure for table `users`
--


/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-04 14:16:27