package api.controller;

import api.adminpayload.TierDTO;
import api.adminpayload.TiersDTO;
import api.adminpayload.UserDTO;
import api.adminpayload.UsersDTO;
import api.exception.EntityNotFoundException;
import api.payload.CreateUserDTO;
import api.payload.SoftPatchUserDTO;
import api.service.TierService;
import api.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/api/tiers")
public class TierController {
    @Autowired
    TierService tierService;

    @GetMapping(value = "")
    public ResponseEntity<?> getUsers() {
        TiersDTO result = tierService.getTiers();
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/{tierId}", method=RequestMethod.GET)
    public ResponseEntity<?> getUser(@PathVariable("tierId") long tierId) {
        try{
            TierDTO result = tierService.getTier(tierId);
            return ResponseEntity.ok(result);
        }
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @RequestMapping(value = "/{tierId}", method=RequestMethod.DELETE)
    public ResponseEntity<?> deleteUser(@PathVariable("tierId") long tierId) {
        try{
            TierDTO result = tierService.deleteTier(tierId);
            return ResponseEntity.ok(result);
        }
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @PostMapping("")
    public ResponseEntity<?> createUser(@Valid @RequestBody TierDTO tierDTO) {
        try{
            TierDTO result = tierService.createTier(tierDTO);
            URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/tiers/{id}").buildAndExpand(result.getId()).toUri();
            return ResponseEntity.created(location).body(result);
        }
        catch(EntityNotFoundException e){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }
    @RequestMapping(value = "/{tierId}", method=RequestMethod.PATCH)
    public ResponseEntity<?> patchTier(@PathVariable("tierId") long tierId,@Valid @RequestBody TierDTO tierDTO) {
        try{
            TierDTO result = tierService.patchTier(tierId,tierDTO);
            return ResponseEntity.ok(result);
        }
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }
}
