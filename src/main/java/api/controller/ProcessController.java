package api.controller;

import api.adminpayload.ProcessDTO;
import api.exception.EntityNotFoundException;

import api.service.ProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/api/processes")
public class ProcessController {
    @Autowired
    ProcessService processService;

    @GetMapping(value = "")
    public ResponseEntity<?> getProcesses() {
        return ResponseEntity.ok(processService.getProcesses());
    }

    @RequestMapping(value = "/{processId}", method= RequestMethod.GET)
    public ResponseEntity<?> getProcess(@PathVariable("processId") long processId) {
        try{
            return ResponseEntity.ok( processService.getProcess(processId));
        }
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @RequestMapping(value = "/{processId}", method=RequestMethod.DELETE)
    public ResponseEntity<?> deleteProcess(@PathVariable("processId") long processId) {
        try{
            return ResponseEntity.ok(processService.deleteProcess(processId));
        }
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @RequestMapping(value = "/{processId}", method=RequestMethod.PATCH)
    public ResponseEntity<?> patchProcess(@PathVariable("processId") long processId,@Valid @RequestBody ProcessDTO processDTO) {
        try{
            return ResponseEntity.ok(processService.patchProcess(processId,processDTO));
        }
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }


    @PostMapping("")
    public ResponseEntity<?> createProcess(@Valid @RequestBody ProcessDTO processDTO) {
        try{
            ProcessDTO result = processService.createProcess(processDTO);
            URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/processes/{id}").buildAndExpand(result.getId()).toUri();
            return ResponseEntity.created(location).body(result);
        }
        catch(EntityNotFoundException e){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @GetMapping(value = "/visible-processes")
    public ResponseEntity<?> getVisibleProcesses() {
        return ResponseEntity.ok(processService.getVisibleProcesses());
    }

}
