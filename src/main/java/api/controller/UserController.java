package api.controller;

import api.adminpayload.ContractTierDTO;
import api.adminpayload.UserCapacityDTO;
import api.adminpayload.UserDTO;
import api.adminpayload.UsersDTO;
import api.exception.ContractException;
import api.exception.EntityNotFoundException;
import api.payload.ApiResponse;
import api.payload.ContractTierRequest;
import api.payload.CreateUserDTO;
import api.payload.SoftPatchUserDTO;
import api.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.Set;

@RestController
@RequestMapping("/api/users")
public class UserController {
    @Autowired
    UserService userService;

    @GetMapping(value = "")
    public ResponseEntity<?> getUsers() {
        UsersDTO result = userService.getUsers();
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/session-capacities", method=RequestMethod.GET)
    public ResponseEntity<?> getSessionCapacities() {
        Set<UserCapacityDTO> result = userService.loadAllActualCapacities();
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/{userId}", method=RequestMethod.GET)
    public ResponseEntity<?> getUser(@PathVariable("userId") long userId) {
        try{
            UserDTO result = userService.getUser(userId);
            return ResponseEntity.ok(result);
        }
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @RequestMapping(value = "/session-user", method=RequestMethod.GET)
    public ResponseEntity<?> getSessionUser() {
        try{
            UserDTO result = userService.getSession();
            return ResponseEntity.ok(result);
        }
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @RequestMapping(value = "/{userId}", method=RequestMethod.DELETE)
    public ResponseEntity<?> deleteUser(@PathVariable("userId") long userId) {
        try{
            UserDTO result = userService.deleteUser(userId);
            return ResponseEntity.ok(result);
        }
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @RequestMapping(value = "/{userId}", method=RequestMethod.PATCH)
    public ResponseEntity<?> softPatchUser(@PathVariable("userId") long userId,@Valid @RequestBody SoftPatchUserDTO softPatchUserDTO) {
        try{
            UserDTO result = userService.patchUser(userId,softPatchUserDTO);
            return ResponseEntity.ok(result);
        }
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @RequestMapping(value = "/owned-users", method=RequestMethod.GET)
    public ResponseEntity<?> getOwnedUsers() {
        return ResponseEntity.ok(userService.getOwnedUsers());
    }

    @PostMapping("")
    public ResponseEntity<?> createUser(@Valid @RequestBody CreateUserDTO createUserDTO) {
      try{
          UserDTO result = userService.createUser(createUserDTO);
          URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/users/{id}").buildAndExpand(result.getId()).toUri();
        return ResponseEntity.created(location).body(result);
      }
      catch(EntityNotFoundException e){
          throw new ResponseStatusException(
                  HttpStatus.NOT_FOUND, e.getMessage(), e);
      }
    }

    @RequestMapping(value = "/{userId}/affiliate", method=RequestMethod.POST)
    public ResponseEntity<?> affiliateUser(@PathVariable("userId") long userId, @Valid @RequestBody ContractTierRequest contractTierRequest) {
        ContractTierDTO result;
        try{
            result = userService.affiliateUser(userId,contractTierRequest);
            URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/users/{id}").buildAndExpand(userId).toUri();
            return ResponseEntity.created(location).body(new ApiResponse(true, "User affiliated successfully"));
        }
        catch(EntityNotFoundException e){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
        catch(ContractException e){
            throw new ResponseStatusException(
                    HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage(), e);
        }
    }
}
