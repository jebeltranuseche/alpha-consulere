package api.controller;

import api.adminpayload.RequirementDTO;
import api.exception.EntityNotFoundException;
import api.service.RequirementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/api/requirements")
public class RequirementController {
    @Autowired
    RequirementService requirementService;

    @GetMapping(value = "")
    public ResponseEntity<?> getRequirements() {
        return ResponseEntity.ok(requirementService.getRequirements());
    }

    @RequestMapping(value = "/{requirementId}", method= RequestMethod.GET)
    public ResponseEntity<?> getRequirement(@PathVariable("requirementId") long requirementId) {
        try{
            return ResponseEntity.ok( requirementService.getRequirement(requirementId));
        }
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @RequestMapping(value = "/{requirementId}", method=RequestMethod.DELETE)
    public ResponseEntity<?> deleteRequirement(@PathVariable("requirementId") long requirementId) {
        try{
            return ResponseEntity.ok(requirementService.deleteRequirement(requirementId));
        }
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @RequestMapping(value = "/{requirementId}", method=RequestMethod.PATCH)
    public ResponseEntity<?> patchRequirement(@PathVariable("requirementId") long requirementId,@Valid @RequestBody RequirementDTO requirementDTO) {
        try{
            return ResponseEntity.ok(requirementService.patchRequirement(requirementId,requirementDTO));
        }
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }


    @PostMapping("")
    public ResponseEntity<?> createRequirement(@Valid @RequestBody RequirementDTO requirementDTO) {
        try{
            RequirementDTO result = requirementService.createRequirement(requirementDTO);
            URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/requirements/{id}").buildAndExpand(result.getId()).toUri();
            return ResponseEntity.created(location).body(result);
        }
        catch(EntityNotFoundException e){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @GetMapping(value = "/visible-requirements")
    public ResponseEntity<?> getVisibleRequirements() {
        return ResponseEntity.ok(requirementService.getVisibleRequirements());
    }

}
