package api.controller;

import api.adminpayload.AuditorTeamDTO;
import api.adminpayload.ProcessDTO;
import api.exception.EntityNotFoundException;
import api.service.AuditorTeamService;
import api.service.ProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/api/auditor-teams")
public class AuditorTeamController {
    @Autowired
    AuditorTeamService auditorTeamService;

    @GetMapping(value = "")
    public ResponseEntity<?> getAuditorTeams() {
        return ResponseEntity.ok(auditorTeamService.getAuditorTeams());
    }

    @RequestMapping(value = "/{auditorTeamId}", method= RequestMethod.GET)
    public ResponseEntity<?> getAuditorTeam(@PathVariable("auditorTeamId") long auditorTeamId) {
        try{
            return ResponseEntity.ok( auditorTeamService.getAuditorTeam(auditorTeamId));
        }
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @RequestMapping(value = "/{auditorTeamId}", method=RequestMethod.DELETE)
    public ResponseEntity<?> deleteAuditorTeam(@PathVariable("auditorTeamId") long auditorTeamId) {
        try{
            return ResponseEntity.ok(auditorTeamService.deleteAuditorTeam(auditorTeamId));
        }
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @RequestMapping(value = "/{auditorTeamId}", method=RequestMethod.PATCH)
    public ResponseEntity<?> patchAuditorTeam(@PathVariable("auditorTeamId") long auditorTeamId,@Valid @RequestBody AuditorTeamDTO auditorTeamDTO) {
        try{
            return ResponseEntity.ok(auditorTeamService.patchAuditorTeam(auditorTeamId,auditorTeamDTO));
        }
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }


    @PostMapping("")
    public ResponseEntity<?> createAuditorTeam(@Valid @RequestBody AuditorTeamDTO auditorTeamDTO) {
        try{
            AuditorTeamDTO result = auditorTeamService.createAuditorTeam(auditorTeamDTO);
            URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/auditor-teams/{id}").buildAndExpand(result.getId()).toUri();
            return ResponseEntity.created(location).body(result);
        }
        catch(EntityNotFoundException e){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }
}
