package api.controller;


import api.adminpayload.DepartmentDTO;
import api.exception.EntityNotFoundException;

import api.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/api/departments")
public class DepartmentController {
    @Autowired
    DepartmentService departmentService;

    @GetMapping(value = "")
    public ResponseEntity<?> getDepartments() {
        return ResponseEntity.ok(departmentService.getDepartments());
    }

    @RequestMapping(value = "/{departmentId}", method= RequestMethod.GET)
    public ResponseEntity<?> getDepartment(@PathVariable("departmentId") long departmentId) {
        try{
            return ResponseEntity.ok( departmentService.getDepartment(departmentId));
        }
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @RequestMapping(value = "/{departmentId}", method=RequestMethod.DELETE)
    public ResponseEntity<?> deleteDepartment(@PathVariable("departmentId") long departmentId) {
        try{
            return ResponseEntity.ok(departmentService.deleteDepartment(departmentId));
        }
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @RequestMapping(value = "/{departmentId}", method=RequestMethod.PATCH)
    public ResponseEntity<?> patchDepartment(@PathVariable("departmentId") long departmentId,@Valid @RequestBody DepartmentDTO departmentDTO) {
        try{
            return ResponseEntity.ok(departmentService.patchDepartment(departmentId,departmentDTO));
        }
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }


    @PostMapping("")
    public ResponseEntity<?> createDepartment(@Valid @RequestBody DepartmentDTO departmentDTO) {
        try{
            DepartmentDTO result = departmentService.createDepartment(departmentDTO);
            URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/departments/{id}").buildAndExpand(result.getId()).toUri();
            return ResponseEntity.created(location).body(result);
        }
        catch(EntityNotFoundException e){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @GetMapping(value = "/visible-departments")
    public ResponseEntity<?> getVisibleDepartments() {
        return ResponseEntity.ok(departmentService.getVisibleDepartments());
    }
}
