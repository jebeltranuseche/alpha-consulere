package api.controller;

import api.adminpayload.*;
import api.exception.EntityNotFoundException;

import api.payload.SoftPatchUserDTO;
import api.service.CompanyService;
import api.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.Set;

@RestController
@RequestMapping("/api/companies")
public class CompanyController  {
    @Autowired
    CompanyService companyService;

    @GetMapping(value = "")
    public ResponseEntity<?> getCompanies() {
        return ResponseEntity.ok(companyService.getCompanies());
    }

    @RequestMapping(value = "/{companyId}", method=RequestMethod.GET)
    public ResponseEntity<?> getCompany(@PathVariable("companyId") long companyId) {
        try{
            return ResponseEntity.ok( companyService.getCompany(companyId));
        }
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @RequestMapping(value = "/{companyId}", method=RequestMethod.DELETE)
    public ResponseEntity<?> deleteCompany(@PathVariable("companyId") long companyId) {
        try{
            return ResponseEntity.ok(companyService.deleteCompany(companyId));
        }
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @RequestMapping(value = "/{companyId}", method=RequestMethod.PATCH)
    public ResponseEntity<?> patchCompany(@PathVariable("companyId") long companyId,@Valid @RequestBody CompanyDTO companyDTO) {
        try{
            return ResponseEntity.ok(companyService.patchCompany(companyId,companyDTO));
        }
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }


    @PostMapping("")
    public ResponseEntity<?> createCompany(@Valid @RequestBody CompanyDTO companyDTO) {
        try{
            CompanyDTO result = companyService.createCompany(companyDTO);
            URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/companies/{id}").buildAndExpand(result.getId()).toUri();
            return ResponseEntity.created(location).body(result);
        }
        catch(EntityNotFoundException e){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @GetMapping(value = "/visible-companies")
    public ResponseEntity<?> getVisibleCompanies() {
        return ResponseEntity.ok(companyService.getVisibleCompanies());
    }

    @GetMapping(value = "/{companyId}/departments")
    public ResponseEntity<?> getCompanyDepartments(@PathVariable("companyId") long companyId) {
        return ResponseEntity.ok(companyService.getCompanyDepartments(companyId));
    }

}
