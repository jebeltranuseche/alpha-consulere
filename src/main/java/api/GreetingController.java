package api;

import java.util.concurrent.atomic.AtomicLong;

import api.adminpayload.AuditDTO;
import api.adminpayload.CommercialEntityDTO;
import api.adminpayload.DepartmentDTO;
import api.exception.AppException;
import api.model.tier.BusinessEntity;
import api.payload.SoftPatchUserDTO;
import api.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
public class GreetingController {

    @Autowired
    UserRepository userRepository;

    private static final String template = "Everything works just fine, %s!";
    private final AtomicLong counter = new AtomicLong();

    @RequestMapping("/greeting")
    public Greeting greeting(@RequestParam(value="name", defaultValue="World") String name) {
        return new Greeting(counter.incrementAndGet(),
                String.format(template, name));
    }

    @RequestMapping("/proof")
    public void proof(HttpServletRequest request) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        System.out.println(authentication.getName());
        userRepository.findByUsername(authentication.getName()).orElseThrow(()->new AppException("a")).getChildren(BusinessEntity.USER);

    }

    @RequestMapping(value = "/", method=RequestMethod.POST)
    public ResponseEntity<?> getSomeShit(@Valid @RequestBody DepartmentDTO departmentDTO) {
        return ResponseEntity.ok( departmentDTO);
    }
}