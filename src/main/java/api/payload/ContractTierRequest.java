package api.payload;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

public class ContractTierRequest {

    @NotNull
    private @DateTimeFormat(pattern="yyyy-MM-dd")
    Date contractEnding;

    @NotNull
    long tierId;

    public Date getContractEnding() {
        return contractEnding;
    }

    public long getTierId() {
        return tierId;
    }
}
