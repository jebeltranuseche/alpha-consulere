package api.payload;

import api.adminpayload.RoleDTO;
import api.model.role_based_access.Role;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class JwtAuthenticationResponse {
    private String username;
    private String accessToken;
    private String tokenType;
    private Collection<GrantedAuthority> authorities;
    private Long id;

    public JwtAuthenticationResponse(String accessToken, String tokenType, String username,Collection<GrantedAuthority> authorities, Long id) {
        this.accessToken = accessToken;
        this.tokenType = tokenType;
        this.username = username;
        this.authorities = authorities;
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public Collection<GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public Long getId() {
        return id;
    }
}
