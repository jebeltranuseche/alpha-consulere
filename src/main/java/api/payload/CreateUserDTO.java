package api.payload;

public class CreateUserDTO extends BasicUserDTO {
    private long ownerId;
    private ContractTierRequest contractTierRequest;

    private long[] rolesIds;

    public long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(long ownerId) {
        this.ownerId = ownerId;
    }

    public void setRolesIds(long[] roles) {
        this.rolesIds = rolesIds;
    }

    public long[] getRolesIds() {
        return rolesIds;
    }

    public ContractTierRequest getContractTierRequest() {
        return contractTierRequest;
    }
}
