package api.payload;

import java.util.Set;

public class UsersResponse {
    private Set<UserResponse> users;

    public Set<UserResponse> getUsers() {
        return users;
    }

    public void setUsers(Set<UserResponse> users) {
        this.users = users;
    }

    public UsersResponse(Set<UserResponse> users) {
        this.users = users;
    }
}
