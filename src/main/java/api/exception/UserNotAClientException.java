package api.exception;

public class UserNotAClientException extends RuntimeException {
    public UserNotAClientException(String name){
        super("User with username: "+ name + "is not a Client, thus cannot perform the current action");
    }
}
