package api.exception;

public class BadFormattedContractException extends ContractException {
    public BadFormattedContractException(String username){
        super("The passed contract for user: "+username+" is bad formatted, thus not persisted");
    }
}
