package api.exception;

public class UserWithoutTierException extends TierException {
    public UserWithoutTierException(String name){
        super("User with username: "+ name + "does not have any tier, thus cannot perform the current action");
    }
}
