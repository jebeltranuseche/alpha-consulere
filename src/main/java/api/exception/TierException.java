package api.exception;

public class TierException extends RuntimeException {
    public TierException(String message) {
        super(message);
    }
}
