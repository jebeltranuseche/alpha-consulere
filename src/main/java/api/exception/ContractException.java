package api.exception;

public class ContractException extends RuntimeException {
    public ContractException(String message){
        super(message);
    }
}
