package api.exception;

public class UnauthorizedException extends RuntimeException {
    public UnauthorizedException(String username){
        super( username + " is not authorized to do the current action");
    }
}
