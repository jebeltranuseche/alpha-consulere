package api.exception;

public class UsernameAlreadyInUseException extends UserAlreadyExistsException {
    public UsernameAlreadyInUseException(String username){
        super("Username" + username+ "is already in use");
    }
}
