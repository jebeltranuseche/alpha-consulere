package api.exception;

public class EntityWithActiveAudit extends  RuntimeException {
    public EntityWithActiveAudit(String entityName) {
        super("Entity " + entityName +": have an active audit thus the current action cannot be performed" );
    }
}

