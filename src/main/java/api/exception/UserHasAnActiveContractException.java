package api.exception;

public class UserHasAnActiveContractException extends ContractException{
    public UserHasAnActiveContractException(String username){
        super("User with username: "+username+" already has an Active Contract");
    }
}
