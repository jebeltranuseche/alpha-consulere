package api.exception;

public class UserWithoutTierCapacityException extends TierException{
    public UserWithoutTierCapacityException(String name){
        super("User with username: "+ name + "does not have tier capacity in order to perform the current action");
    }
}