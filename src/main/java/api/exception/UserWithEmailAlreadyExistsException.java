package api.exception;

public class UserWithEmailAlreadyExistsException extends UserAlreadyExistsException {
    public UserWithEmailAlreadyExistsException(String email){
        super("User with email: "+ email + "already exists");
    }
}
