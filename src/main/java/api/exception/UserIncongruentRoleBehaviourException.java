package api.exception;

public class UserIncongruentRoleBehaviourException extends RuntimeException {
    public UserIncongruentRoleBehaviourException(String username){
        super("User " + username + " has a role that does not allow current action");
    }
}
