package api.exception;

public class UserWithoutActiveContractException extends ContractException {
    public UserWithoutActiveContractException(String username){
        super("User with username: " + username + "does not have an active contract, thus cannot realize the current action") ;
    }
}
