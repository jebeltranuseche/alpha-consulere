package api.adminpayload;

import api.model.Process;
import api.model.Requirement;

import java.util.HashSet;
import java.util.Set;

public class ProcessDTO  extends  OwnableEntityDTO{
    private String input;
    private String output;
    private String name;
    private Set<RequirementDTO> requirements;
    private long departmentId;
    private Set<Long> requirementsToAdd;
    private Set<Long>  requirementsToRemove;
    private Set<String> requirementsNames;
    public ProcessDTO(Process process, boolean withRelations){
        setBasicInformation(process);
        if(withRelations) setRequirements(process);
    }

    private void setBasicInformation(Process process){
        super.setBaseEntityAttributes(process);
        input = process.getInput();
        output = process.getOutput();
        name = process.getName();
        departmentId = process.getDepartment().getId();
        ownerId = process.getDepartment().getCompany().getId();
        setRequirementsNames(process);
        setRequirementsNames(process);
    }

    public ProcessDTO() {
    }

    private void setRequirements(Process process){
        requirements = new HashSet<>();
        process.getRequirements().forEach(requirement -> requirements.add(requirement.toDTO(false)));
    }

    private void setRequirementsNames( Process process){
        requirementsNames = new HashSet<>();
        process.getRequirements().forEach(requirement -> requirementsNames.add(requirement.getName()));
    }

    public String getInput() {
        return input;
    }

    public String getOutput() {
        return output;
    }

    public String getName() {
        return name;
    }

    public long getDepartmentId() {
        return departmentId;
    }

    public Set<Long> getRequirementsToAdd() {
        return requirementsToAdd;
    }

    public  Set<Long>  getRequirementsToRemove() {
        return requirementsToRemove;
    }

    public Set<String> getRequirementsNames() {
        return requirementsNames;
    }

    public Set<RequirementDTO> getRequirements() {
        return requirements;
    }
}
