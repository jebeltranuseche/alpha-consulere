package api.adminpayload;

import api.model.Appointment;

import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

public class AppointmentDTO extends BaseEntityDTO {
    private Instant appointmentTime;
    private Set<AuditRequirementDTO> auditRequirements;

    public Instant getAppointmentTime() {
        return appointmentTime;
    }

    public Set<AuditRequirementDTO> getAuditRequirements() {
        return auditRequirements;
    }


    public AppointmentDTO(Appointment appointment, boolean withRelations){
        setBasicInformation(appointment);
        if(withRelations) setRelations(appointment);
    }

    private void setBasicInformation(Appointment appointment) {
        appointmentTime = appointment.getAppointmentTime();
    }

    private  void setRelations(Appointment appointment){
        setAuditRequirements(appointment);
    }

    private void setAuditRequirements(Appointment appointment){
        auditRequirements = new HashSet<>();
        appointment.getAuditRequirements().forEach(auditRequirement -> auditRequirement.toDTO(false));
    }

    public AppointmentDTO() {
    }
}
