package api.adminpayload;

import api.model.tier.Tier;

import java.util.HashSet;
import java.util.Set;

public class TiersDTO {
    private Set<TierDTO> tiers;
    public TiersDTO(Set<Tier> tiers){
        this.tiers = new HashSet<>();
        tiers.forEach((tier)-> { this.tiers.add(new TierDTO(tier));});
    }

    public Set<TierDTO> getTiers() {
        return tiers;
    }

    public TiersDTO(){

    }
}
