package api.adminpayload;

import api.model.user.User;
import api.model.role_based_access.Role;
import api.model.tier.ContractTier;

import java.util.HashSet;
import java.util.Set;

public class UserDTO extends BaseEntityDTO{
    private String username;
    private String email;
    private Set<RoleDTO> roles;
    private Set<ContractTierDTO> contracts;
    private ContractTierDTO activeContract;
    private UserDTO owner;
    private long ownerId;
    private Set<UserDTO> users;
    private ProfileDTO profile;

    public UserDTO(User user, boolean withRelations){
        setForUser(user, withRelations);
    }

    public UserDTO(){
        super();
    }

    public void setForUser(User user, boolean withRelations){
        setForBasicUserInfo(user);
        if(withRelations)setRelations(user);
    }

    public ProfileDTO getProfile() {
        return profile;
    }

    private void setRelations(User user){
        setUsers(user.getOwnedUsers());
        if(user.getOwner()!=null) setOwner(user.getOwner());
        setContracts(user.getContracts());
    }
    public void setForBasicUserInfo(User user){
        this.id = user.getId();
        this.username = user.getUsername();
        this.email = user.getEmail();
        if(user.getOwner()  != null) this.ownerId = user.getOwner().getId();
        super.setBaseEntityAttributes(user);
        setRoles(user.getRoles());
        if(user.getProfile()!= null) this.profile = new ProfileDTO(user.getProfile());
    }

    public void setForChildUser(User user){
        setForBasicUserInfo(user);
    }

    public void setRoles(Set<Role> roles){
        this.roles = new HashSet<RoleDTO>();
        roles.forEach(role -> {this.roles.add(new RoleDTO(role));});
    }

    public void setUsers(Set<User> users){
        this.users = new HashSet<UserDTO>();
        users.forEach(user -> {
            UserDTO userDTO = new UserDTO();
            userDTO.setForChildUser(user);
            this.users.add(userDTO);
        });
    }

    public void setContracts(Set<ContractTier> contracts){
        this.contracts = new HashSet<ContractTierDTO>();
        contracts.forEach(contract -> {
            ContractTierDTO contractDTO = new ContractTierDTO();
            contractDTO.setForUserContract(contract);
            this.contracts.add(contractDTO);
        });
    }

    public void setOwner(User owner){
        this.owner = new UserDTO();
        this.owner.setForBasicUserInfo(owner);
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public Set<RoleDTO> getRoles() {
        return roles;
    }

    public Set<ContractTierDTO> getContracts() {
        return contracts;
    }

    public ContractTierDTO getActiveContract() {
        return activeContract;
    }

    public UserDTO getOwner() {
        return owner;
    }

    public Set<UserDTO> getUsers() {
        return users;
    }

    public long getOwnerId() {
        return ownerId;
    }
}
