package api.adminpayload;

import api.model.Company;
import api.model.Department;
import api.model.base_entity.LeadedEntity;

import java.util.HashSet;
import java.util.Set;

public class DepartmentDTO extends CommercialEntityDTO {
    private String name;
    private Set<ProcessDTO> processes;
    private Set<AuditDTO> audits;
    private CompanyDTO company;
    private Set<RequirementDTO> requirements;
    private long companyId;

    public DepartmentDTO(Department department , boolean withRelations){
        setBasicInformation(department);
        if(withRelations) setRelations(department);
    }

    public DepartmentDTO(){}

    private void setBasicInformation(Department department ){
        super.setCommercialEntityAttributes(department);
        companyId = department.getCompany().getId();
        name = department.getName();
        setCompany(department);
    }

    private void setRelations(Department department){
        setProcesses(department, true);
        setRequirements(department,true);
    }

    private void setCompany(Department department){
        company = department.getCompany().toDTO(false);
    }

    private  void setAudits(Department department){
        audits = new HashSet<>();
        department.getAudits().forEach(audit -> audits.add(audit.toDTO(false)));
    }

    private  void setProcesses(Department department, boolean withRelations){
        processes = new HashSet<>();
        department.getProcesses().forEach(process -> processes.add(process.toDTO(withRelations)));
    }

    private void setRequirements(Department department, boolean withRelations){
       requirements = new HashSet<>();
       department.getRequirements().forEach(requirement -> requirements.add(requirement.toDTO(withRelations)));
    }

    public String getName() {
        return name;
    }

    public Set<ProcessDTO> getProcesses() {
        return processes;
    }

    public Set<AuditDTO> getAudits() {
        return audits;
    }

    public CompanyDTO getCompany() {
        return company;
    }

    public long getCompanyId(){ return companyId; }

    public Set<RequirementDTO> getRequirements() {
        return requirements;
    }

}
