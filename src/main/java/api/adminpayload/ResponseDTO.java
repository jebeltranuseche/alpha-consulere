package api.adminpayload;

import api.model.Response;

import java.time.Instant;

public class ResponseDTO extends BaseEntityDTO {
    private String correction;
    private String correctiveAction;
    private Instant responseTime;
    private String auditorResponse;
    private String status;

    public ResponseDTO(Response response){
        setBasicInformation(response);
    }

    private void setBasicInformation(Response response){
        correction = response.getCorrection();
        correctiveAction = response.getCorrectiveAction();
        responseTime = response.getResponseTime();
        auditorResponse = response.getAuditorResponse();
        status =  response.getStatus().toString();
    }
}
