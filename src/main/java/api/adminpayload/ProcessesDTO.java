package api.adminpayload;

import api.model.Process;

import java.util.HashSet;
import java.util.Set;

public class ProcessesDTO {
    private Set<ProcessDTO> processes;

    public Set<ProcessDTO> getProcesses() {
        return processes;
    }

    public  ProcessesDTO(Set<Process> processes, boolean withRelations){
        setProcesses(processes, withRelations );
    }

    private void setProcesses(Set<Process> processes, boolean withRelations){
        this.processes = new HashSet<>();
        processes.forEach(process -> this.processes.add(process.toDTO(withRelations)));
    }

    public ProcessesDTO() {
    }

}
