package api.adminpayload;

import api.model.tier.TierCapacity;

public class TierCapacityDTO extends BaseEntityDTO{
    private String entity;
    private int capacity;

    public TierCapacityDTO() {
    }

    public String getEntity() {
        return entity;
    }

    public int getCapacity() {
        return capacity;
    }

    public TierCapacityDTO(TierCapacity tierCapacity) {
        this.entity = tierCapacity.getEntity();
        this.capacity = tierCapacity.getCapacity();
    }
}
