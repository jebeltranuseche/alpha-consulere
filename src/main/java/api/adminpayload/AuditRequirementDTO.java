package api.adminpayload;

import api.model.Audit;
import api.model.AuditRequirement;
import api.model.Requirement;

import java.util.HashSet;
import java.util.Set;

public class AuditRequirementDTO extends BaseEntityDTO {
    private Set<ProcessDTO> processes;
    private Set<AppointmentDTO> appointments;
    private RequirementDTO requirement;
    private NonConformityDTO nonConformity;

    public AuditRequirementDTO(AuditRequirement auditRequirement, boolean withRelations){
        setBasicInformation(auditRequirement);
        if(withRelations) setRelations(auditRequirement);
    }

    private void setBasicInformation(AuditRequirement auditRequirement){
        setNonConformity(auditRequirement);
        setProcesses(auditRequirement);
        setRequirement(auditRequirement);
    }

    private void setRelations(AuditRequirement auditRequirement){
        setAppointments(auditRequirement);
    }

    private void setNonConformity(AuditRequirement auditRequirement){
        nonConformity = auditRequirement.getNonConformity().toDTO(false);
    }

    private void setRequirement(AuditRequirement auditRequirement){
        requirement = auditRequirement.getRequirement().toDTO(false);
    }

    private void setAppointments(AuditRequirement auditRequirement){
        appointments = new HashSet<>();
        auditRequirement.getAppointments().forEach(appointment -> appointments.add(appointment.toDTO(false)));
    }

    private void setProcesses(AuditRequirement auditRequirement){
        processes = new HashSet<>();
        auditRequirement.getProcesses().forEach(process -> processes.add(process.toDTO(false)));
    }

    public AuditRequirementDTO() {
    }
}
