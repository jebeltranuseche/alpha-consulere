package api.adminpayload;

import api.model.AuditorTeam;

import java.util.HashSet;
import java.util.Set;

public class AuditorTeamsDTO {
    private Set<AuditorTeamDTO> auditorTeams;
    public AuditorTeamsDTO(Set<AuditorTeam> auditorTeams, boolean withRelations){
        setAuditorTeams(auditorTeams,withRelations);
    }

    private void setAuditorTeams(Set<AuditorTeam> auditorTeams, boolean withRelations){
        this.auditorTeams = new HashSet<>();
        auditorTeams.forEach(auditorTeam -> this.auditorTeams.add(auditorTeam.toDTO(withRelations)));
    }

    public AuditorTeamsDTO() {

    }
}
