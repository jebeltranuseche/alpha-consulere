package api.adminpayload;

import api.model.user.User;

import java.util.HashSet;
import java.util.Set;

public class UsersDTO {
    Set<UserDTO> users;

    public UsersDTO(){

    }

    public UsersDTO(Set<User> users){
        setUsers(users);
    }

    public void setUsers(Set<User> users){
        this.users = new HashSet<UserDTO>();
        users.forEach(user -> this.users.add(user.toDTO(false)));
    }

    public void setUsersBasicInformation(Set<User> users) {
        this.users = new HashSet<UserDTO>();
        users.forEach(user -> {
            UserDTO userDTO = new UserDTO();
            userDTO.setForBasicUserInfo(user);
            this.users.add(userDTO);
        });
    }

    public Set<UserDTO> getUsers() {
        return users;
    }
}
