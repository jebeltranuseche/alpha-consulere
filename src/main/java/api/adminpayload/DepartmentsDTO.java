package api.adminpayload;

import api.model.Department;

import java.util.HashSet;
import java.util.Set;

public class DepartmentsDTO {
    private Set<DepartmentDTO> departments;

    public DepartmentsDTO(Set<Department> departments, boolean withRelations){
        setDepartments(departments, withRelations);
    }

    private void setDepartments(Set<Department> departments, boolean withRelations){
        this.departments = new HashSet<>();
        departments.forEach(department -> this.departments.add(department.toDTO(withRelations)));
    }

    public DepartmentsDTO() {

    }

    public Set<DepartmentDTO> getDepartments() {
        return departments;
    }
}
