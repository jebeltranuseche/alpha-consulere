package api.adminpayload;

public class CommercialResponsableDTO {
    private long responsableId;
    public CommercialResponsableDTO(long responsableId){
        this.responsableId = responsableId;
    }

    public long getResponsableId() {
        return responsableId;
    }


}
