package api.adminpayload;

public class OwnableEntityDTO extends  BaseEntityDTO {
    protected UserDTO owner;
    protected long ownerId;

    public UserDTO getOwner() {
        return owner;
    }

    public long getOwnerId() {
        return ownerId;
    }
}
