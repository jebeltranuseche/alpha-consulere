package api.adminpayload;

import api.model.Requirement;

import java.util.HashSet;
import java.util.Set;

public class RequirementsDTO {
    private Set<RequirementDTO> requirements;
    public RequirementsDTO(Set<Requirement> requirements, boolean withRelations){
        setCompanies(requirements,withRelations);
    }

    private void setCompanies(Set<Requirement> requirements, boolean withRelations){
        this.requirements = new HashSet<>();
        requirements.forEach(requirement -> this.requirements.add(requirement.toDTO(withRelations)));
    }

    public RequirementsDTO() {
    }

    public Set<RequirementDTO> getRequirements() {
        return requirements;
    }
}
