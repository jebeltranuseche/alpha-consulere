package api.adminpayload;

import api.model.base_entity.CommercialEntity;
import api.model.user.User;

public abstract class CommercialEntityDTO  extends BaseEntityDTO{
    protected UserDTO owner;
    protected UserDTO leader;
    private long ownerId;
    private long leaderId;
    private boolean unsetLeader;

    public CommercialEntityDTO(CommercialEntity commercialEntity){
        super(commercialEntity);
        setCommercialEntityAttributes(commercialEntity);
    }
    public CommercialEntityDTO(){

    }

    public void setCommercialEntityAttributes(CommercialEntity commercialEntity  ){
            this.owner = new UserDTO();
            this.owner.setForBasicUserInfo(commercialEntity.getOwner());
            this.id = commercialEntity.getId();
            ownerId = commercialEntity.getOwner().getId();
            if(commercialEntity.getLeader() != null){
            leaderId = commercialEntity.getLeader().getId();
            this.leader = new UserDTO();
            this.leader.setForBasicUserInfo(commercialEntity.getLeader());
        }
    }

    public UserDTO getOwner() {
        return owner;
    }

    public UserDTO getLeader() {
        return leader;
    }

    public long getOwnerId() {
        return ownerId;
    }

    public long getLeaderId() {
        return leaderId;
    }

    public boolean isUnsetLeader() {
        return unsetLeader;
    }
}
