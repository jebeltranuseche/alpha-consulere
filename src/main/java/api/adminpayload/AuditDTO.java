package api.adminpayload;

import api.model.Audit;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class  AuditDTO extends CommercialEntityDTO {
    private String name;
    private String status;
    private Date iniDate;
    private Date endDate;
    private Set<AuditRequirementDTO> inspections;
    private AuditorTeamDTO auditorTeam;
    private Set<AppointmentDTO> appointments;
    private DepartmentDTO department;

    public String getName() {
        return name;
    }

    public String getStatus() {
        return status;
    }

    public Date getIniDate() {
        return iniDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public AuditDTO(Audit audit, boolean withRelations){
        setBasicInformation(audit);
        if(withRelations) setRelations(audit);
    }

    public AuditDTO(){

    }

    private void setBasicInformation(Audit audit) {
        super.setCommercialEntityAttributes(audit);
        name = audit.getName();
        status = audit.getStatus().toString();
        iniDate = audit.getIniDate();
        endDate = audit.getEndDate();
    }

    private void setRelations(Audit audit){
        auditorTeam = audit.getAuditorTeam().toDTO(false);
        department = audit.getDepartment().toDTO(false);


    }

    private void setAppointments(Audit audit){
        appointments = new HashSet<>();
        audit.getAppointments().forEach(appointment -> appointment.toDTO(true));
    }
    public Set<AuditRequirementDTO> getInspections() {
        return inspections;
    }

    public AuditorTeamDTO getAuditorTeam() {
        return auditorTeam;
    }

    public DepartmentDTO getDepartment() {
        return department;
    }
}
