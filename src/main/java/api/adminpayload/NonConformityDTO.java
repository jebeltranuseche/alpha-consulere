package api.adminpayload;

import api.model.NonConformity;
import api.model.Response;

import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

public class NonConformityDTO extends BaseEntityDTO {
    private String status;
    private Instant nonConformityTime;
    private Set<ResponseDTO> proposedResponses;
    private ResponseDTO effectiveResponse;


    public NonConformityDTO(NonConformity nonConformity){
        setBasicInformation(nonConformity);
    }

    private void setBasicInformation (NonConformity nonConformity){
       status = nonConformity.getStatus().toString();
       nonConformityTime = nonConformity.getNonConformityTime();
       if(nonConformity.getEffectiveResponse() != null) effectiveResponse = new ResponseDTO(nonConformity.getEffectiveResponse());
       proposedResponses = new HashSet<>();
       nonConformity.getProposedResponses().forEach(response -> proposedResponses.add(new ResponseDTO(response)));
    }

}
