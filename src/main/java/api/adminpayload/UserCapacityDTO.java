package api.adminpayload;

public class UserCapacityDTO {
    private String entity;
    private int capacity;

    public UserCapacityDTO(String entity, int capacity) {
        this.entity = entity;
        this.capacity = capacity;
    }

    public String getEntity() {
        return entity;
    }

    public int getCapacity() {
        return capacity;
    }

}
