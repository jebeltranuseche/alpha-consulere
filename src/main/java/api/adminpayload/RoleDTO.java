package api.adminpayload;

import api.model.role_based_access.Role;

import java.time.Instant;

public class RoleDTO extends BaseEntityDTO {
    private String name;

    public RoleDTO(Role role) {
        super(role);
        this.name = role.getName();
        this.id = role.getId();
    }

    public String getName() {
        return name;
    }
}
