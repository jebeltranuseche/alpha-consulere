package api.adminpayload;

import api.model.tier.Tier;

import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

public class TierDTO extends BaseEntityDTO {
    private String name;
    private long id;
    private Set<TierCapacityDTO> tierCapacities;
    public TierDTO(){
    }

    public Set<TierCapacityDTO> getTierCapacities() {
        return tierCapacities;
    }

    @Override
    public long getId() {
        return id;
    }

    public TierDTO(Tier tier) {
        super(tier);
        name = tier.getName();
        id = tier.getId();
        tierCapacities  = new HashSet<>();
        tier.getTierCapacities().forEach(tierCapacity -> tierCapacities.add(new TierCapacityDTO(tierCapacity)));;
    }

    public String getName() {
        return name;
    }

    public void setName(String name){ this.name = name; }

    public void setTier(Tier tier){
        this.name = tier.getName();
        this.id = tier.getId();
        this.tierCapacities = new HashSet<>();
        tier.getTierCapacities().forEach((tierCapacity)->{this.tierCapacities.add(new TierCapacityDTO(tierCapacity));});
    }

    public void setTierCapacities(Set<TierCapacityDTO> tierCapacities){ this.tierCapacities = tierCapacities; }
}
