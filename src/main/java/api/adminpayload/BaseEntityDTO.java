package api.adminpayload;

import api.model.base_entity.BaseEntity;

import java.time.Instant;

public abstract class BaseEntityDTO{
    private Long updatedBy;
    private Long createdBy;
    private Instant createdAt;
    private Instant updatedAt;
    protected  long id;

    public BaseEntityDTO(BaseEntity baseEntity) {
        setBaseEntityAttributes(baseEntity);
    }

    public BaseEntityDTO(){

    }

    public void setBaseEntityAttributes(BaseEntity baseEntity){
        this.updatedBy = baseEntity.getUpdatedBy();
        this.createdBy = baseEntity.getCreatedBy();
        this.createdAt = baseEntity.getCreatedAt();
        this.updatedAt = baseEntity.getUpdatedAt();
        this.id = baseEntity.getId();
    }

    public Long getUpdatedBy() {
        return updatedBy;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public Instant getUpdatedAt() {
        return updatedAt;
    }

    public long getId(){ return id;}
}
