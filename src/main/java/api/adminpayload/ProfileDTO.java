package api.adminpayload;

import api.model.user.Profile;

public class ProfileDTO {
    private String firstName;
    private String lastName;
    private String identificationDocument;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setDNI(String identificationDocument) {
        this.identificationDocument = identificationDocument;
    }

    public String getLastName() {
        return lastName;
    }

    public String getIdentificationDocument() {
        return identificationDocument;
    }

    public ProfileDTO(String firstName, String lastName, String identificationDocument) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.identificationDocument = identificationDocument;
    }

    public ProfileDTO(Profile profile) {
        this.firstName = profile.getFirstName();
        this.lastName = profile.getLastName();
        this.identificationDocument = profile.getDni();
    }

    public ProfileDTO() {

    }
}
