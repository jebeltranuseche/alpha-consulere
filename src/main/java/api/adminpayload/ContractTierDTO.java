package api.adminpayload;

import api.model.tier.ContractTier;

import java.time.Instant;

public class ContractTierDTO extends BaseEntityDTO {
    private TierDTO tier;
    private UsersDTO user;
    private Instant endDate;
    private Long id;

    public ContractTierDTO(ContractTier contractTier) {
        super(contractTier);
        this.id = id;
        this.tier = new TierDTO(contractTier.getTier());
        this.endDate = contractTier.getEndDate();
        this.user = user;
    }

    public ContractTierDTO(){

    }

    public void setForUserContract(ContractTier contractTier){
        this.id = contractTier.getId();
        this.tier = new TierDTO(contractTier.getTier());
        this.endDate = contractTier.getEndDate();
        super.setBaseEntityAttributes(contractTier);
    }

    public TierDTO getTier() {
        return tier;
    }

    public UsersDTO getUser() {
        return user;
    }

    public Instant getEndDate() {
        return endDate;
    }

    @Override
    public long getId() {
        return id;
    }
}
