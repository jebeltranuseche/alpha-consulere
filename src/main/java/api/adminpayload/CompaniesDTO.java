package api.adminpayload;

import api.model.Company;

import java.util.HashSet;
import java.util.Set;

public class CompaniesDTO {
    private Set<CompanyDTO> companies;
    public CompaniesDTO(Set<Company> companies, boolean withRelations){
        setCompanies(companies,withRelations);
    }

    private void setCompanies(Set<Company> companies, boolean withRelations){
        this.companies = new HashSet<>();
        companies.forEach(company -> this.companies.add(company.toDTO(withRelations)));
    }

    public CompaniesDTO() {
    }

    public Set<CompanyDTO> getCompanies() {
        return companies;
    }
}

