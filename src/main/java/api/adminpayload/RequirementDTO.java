package api.adminpayload;

import api.model.Department;
import api.model.Requirement;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RequirementDTO extends OwnableEntityDTO {
    private String name;
    private String description;
    private RequirementDTO filterRequirement;
    private DepartmentDTO department;
    private long departmentId;
    private long filterRequirementId;
    private Set<Long>  processesToAdd;
    private Set<Long> processesToRemove;
    private Set<String> processesNames;
    private Set<ProcessDTO> processes;

    public Set<Long> getProcessesToAdd() {
        return processesToAdd;
    }

    public Set<Long>  getProcessesToRemove() {
        return processesToRemove;
    }


    public RequirementDTO(Requirement requirement, boolean withRelations){
        setBasicInformation(requirement);
        if(withRelations)setRelations(requirement);
    }

    public  RequirementDTO(){

    }

    private void setBasicInformation(Requirement requirement){
        super.setBaseEntityAttributes(requirement);
        name = requirement.getName();
        description = requirement.getDescription();
        departmentId = requirement.getDepartment().getId();
        ownerId = requirement.getDepartment().getCompany().getId();
        setProcessesNames(requirement);
        if(requirement.getFilterRequirement() != null) { setFilterRequirement(requirement); }
    }

    private void setProcessesNames(Requirement requirement){
        processesNames = new HashSet<>();
        requirement.getProcesses().forEach(process -> {
            System.out.println("AADSADAS " + process.getId());
            processesNames.add(process.getName());
        });
    }

    private void setFilterRequirement(Requirement requirement) {
        RequirementDTO filterRequirement = new RequirementDTO();
        filterRequirement.setBasicInformation(requirement.getFilterRequirement());
        this.filterRequirement = filterRequirement;
    }

    private void setDepartment(Requirement requirement){
        this.department = requirement.getDepartment().toDTO(false);
    }
    private void setProcesses(Requirement requirement){
         processes = new HashSet<>();
         requirement.getProcesses().forEach(process -> processes.add(process.toDTO(false)) );
    }

    private void setRelations(Requirement requirement){
        setDepartment(requirement);
        setProcesses(requirement);
    }


    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public long getFilterRequirementId() {
        return filterRequirementId;
    }

    public RequirementDTO getFilterRequirement() {
        return filterRequirement;
    }

    public DepartmentDTO getDepartment() {
        return department;
    }

    public long getDepartmentId() {
        return departmentId;
    }

    public Set<String> getProcessesNames() {
        return processesNames;
    }
}
