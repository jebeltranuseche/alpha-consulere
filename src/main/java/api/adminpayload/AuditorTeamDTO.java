package api.adminpayload;

import api.model.AuditorTeam;

import java.util.HashSet;
import java.util.Set;

public class AuditorTeamDTO extends CommercialEntityDTO {
    private Set<UserDTO> auditors;
    private String name;
    private String bin;
    private long[] auditorsToAdd;
    private long [] getAuditorsToRemove;
    private Set<AuditDTO> audits;

    public AuditorTeamDTO(AuditorTeam auditorTeam, boolean withRelations){
        setBasicInformation(auditorTeam);
        if(withRelations) setRelations(auditorTeam);
    }

    private void setRelations(AuditorTeam auditorTeam){
        setAudits(auditorTeam);
    }

    private void setAudits(AuditorTeam auditorTeam){
        audits = new HashSet<>();
        auditorTeam.getAudits().forEach(audit -> audits.add(audit.toDTO(false)));
    }
    private void setBasicInformation(AuditorTeam auditorTeam){
        name = auditorTeam.getName();
        bin = auditorTeam.getBin();
        auditors = new HashSet<>();
        auditorTeam.getAuditors().forEach(auditor -> auditors.add(auditor.toDTO(false)));
    }

    public Set<UserDTO> getAuditors() {
        return auditors;
    }

    public String getName() {
        return name;
    }

    public String getBin() {
        return bin;
    }

    public long[] getAuditorsToAdd() {
        return auditorsToAdd;
    }

    public long[] getGetAuditorsToRemove() {
        return getAuditorsToRemove;
    }

    public Set<AuditDTO> getAudits() {
        return audits;
    }
}
