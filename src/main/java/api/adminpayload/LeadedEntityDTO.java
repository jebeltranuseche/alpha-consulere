package api.adminpayload;

import api.model.base_entity.LeadedEntity;

public abstract class LeadedEntityDTO extends BaseEntityDTO {
    private long leaderId;
    private UserDTO leader;

    public LeadedEntityDTO(LeadedEntity leadedEntity){
        super(leadedEntity);
        setLeadedEntityAttributes(leadedEntity);
    }
    public LeadedEntityDTO(){

    }

    public void setLeadedEntityAttributes(LeadedEntity leadedEntity){

        this.leader = new UserDTO();
        this.id = leadedEntity.getId();
        if(leadedEntity.getLeader() != null)this.leader.setForBasicUserInfo(leadedEntity.getLeader());
    }

    public UserDTO getLeader() {
        return leader;
    }

    public long getLeaderId() {
        return leaderId;
    }
}
