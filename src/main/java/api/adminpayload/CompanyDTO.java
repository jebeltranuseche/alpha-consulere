package api.adminpayload;

import api.model.Company;

import java.util.HashSet;
import java.util.Set;

public class CompanyDTO extends CommercialEntityDTO {
    private String name;
    private String bin;
    private Set<DepartmentDTO> departments;


    public CompanyDTO(Company company, boolean withRelations){

        setBasicInformation(company);
        if(withRelations) setRelations(company);
    }

    public CompanyDTO(){}

    private void setBasicInformation(Company company){
        super.setCommercialEntityAttributes(company);
        name = company.getName();
        bin = company.getBin();
    }

    private void setRelations(Company company){
        setDepartments(company);
    }

    private void setDepartments(Company company){
        departments = new HashSet<>();
        company.getDepartments().forEach(department -> departments.add(new DepartmentDTO(department, false)) );
    }

    public String getName() {
        return name;
    }

    public String getBin() {
        return bin;
    }

    public Set<DepartmentDTO> getDepartments() {
        return departments;
    }
}
