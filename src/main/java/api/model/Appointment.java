package api.model;

import api.adminpayload.AppointmentDTO;
import api.model.base_entity.BaseEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.Set;

@Entity
@Table(name = "appointments")
public class Appointment extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Instant appointmentTime;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="audit_id")
    private Audit audit;

    @ManyToMany(mappedBy = "appointments",fetch = FetchType.LAZY)
    private Set<AuditRequirement> auditRequirements;


    public Set<AuditRequirement> getAuditRequirements() {
        return auditRequirements;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public AppointmentDTO toDTO(boolean withRelations) {
        return new AppointmentDTO(this,withRelations);
    }

    public Instant getAppointmentTime() {
        return appointmentTime;
    }


    public Appointment(Instant appointmentTime) {
        this.appointmentTime = appointmentTime;
    }

    public Audit getAudit() {
        return audit;
    }

    public Appointment(){

    }
}
