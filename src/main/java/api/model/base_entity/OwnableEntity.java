package api.model.base_entity;

import api.model.user.User;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public  abstract class  OwnableEntity extends BaseEntity{
    public abstract User getOwner();
    public abstract void setOwner(User user);
}
