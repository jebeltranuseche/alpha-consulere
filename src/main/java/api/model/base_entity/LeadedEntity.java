package api.model.base_entity;

import api.model.user.User;

public abstract class LeadedEntity extends BaseEntity {
    public abstract User getLeader();
    public abstract void setLeader(User user);
}
