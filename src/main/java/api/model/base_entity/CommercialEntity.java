package api.model.base_entity;

import api.adminpayload.CommercialEntityDTO;
import api.model.user.User;

public abstract class CommercialEntity extends OwnableEntity {
    public abstract User getLeader();
    public abstract void setLeader(User user);
}
