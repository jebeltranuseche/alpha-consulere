package api.model;

public enum AuditStatus {
    PLANNING, ONGOING, PAUSED, CANCELLED, CLOSED, COMPLETED;
}
