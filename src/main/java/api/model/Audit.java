package api.model;

import api.adminpayload.AuditDTO;
import api.exception.UserIncongruentRoleBehaviourException;
import api.exception.UserWithoutTierCapacityException;
import api.model.base_entity.CommercialEntity;
import api.model.tier.BusinessEntity;
import api.model.user.User;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "audits")
public class Audit extends CommercialEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = 15)
    private String name;

    @NotNull
    private Date iniDate;

    @NotNull
    private Date endDate;

    @NotNull
    @Enumerated(EnumType.STRING)
    private AuditStatus status;

    @OneToMany(mappedBy="audit", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    private Set<Appointment> appointments  = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "evaluation_id")
    private Evaluation evaluation;

    @OneToOne(mappedBy = "audit",cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Report report;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "auditor_team_id")
    private AuditorTeam auditorTeam;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "department_id")
    private Department department;

    @OneToMany(mappedBy = "audit", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<AuditRequirement> auditRequirements = new HashSet<>();

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "owner_id")
    private User owner;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "leader_id")
    private User leader;

    @Override
    public void setOwner(User owner) {
        if(owner != null && !owner.hasRolesForCreation())
            throw new UserIncongruentRoleBehaviourException(owner.getUsername());
        if(owner != null && !owner.verifyCreationAbility(BusinessEntity.USER))
            throw new UserWithoutTierCapacityException(owner.getUsername());
        this.owner = owner;
    }

    public void setLeader(User leader) {
        this.leader = leader;
    }

    @Override
    public User getOwner() {
        return owner;
    }

    @Override
    public User getLeader() {
        return leader;
    }

    @Override
    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Date getIniDate() {
        return iniDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public AuditStatus getStatus() {
        return status;
    }

    public Evaluation getEvaluation() {
        return evaluation;
    }

    public AuditorTeam getAuditorTeam() {
        return auditorTeam;
    }

    public Department getDepartment() {
        return department;
    }

    public Set<AuditRequirement> getAuditRequirements() {
        return auditRequirements;
    }

    public Set<Appointment> getAppointments() {
        return appointments;
    }

    public Report getReport() {
        return report;
    }

    @Override
    public AuditDTO toDTO(boolean withRelations) {
        return new AuditDTO(this, withRelations);
    }

    public boolean isActive(){
        return  status.equals(AuditStatus.ONGOING) ||status.equals(AuditStatus.CLOSED) || status.equals(AuditStatus.COMPLETED);
    }

    public Audit(){

    }
}


