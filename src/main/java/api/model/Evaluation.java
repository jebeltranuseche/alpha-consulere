package api.model;

import api.adminpayload.BaseEntityDTO;
import api.adminpayload.EvaluationDTO;
import api.model.base_entity.CommercialEntity;
import api.model.user.User;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "evaluations")
public class Evaluation extends CommercialEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public EvaluationDTO toDTO(boolean withRelations) {
        return null;
    }

    @NotBlank
    @Size(max = 15)
    private String name;

    @NotNull
    private boolean isCertified;

    @NotBlank
    @Size(max = 256)
    private String description;

    @NotNull
    private Date iniDate;

    @NotNull
    private Date endDate;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="owner_id")
    private User owner;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="leader_id")
    private User leader;

    @OneToMany(mappedBy="evaluation", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<Audit> audits = new HashSet<>();

    @Override
    public void setOwner(User owner) {
        this.owner = owner;
    }

    public void setLeader(User leader) {
        this.leader = leader;
    }

    @Override
    public User getOwner() {
        return owner;
    }

    @Override
    public User getLeader() {
        return leader;
    }

    public Evaluation(){

    }

}
