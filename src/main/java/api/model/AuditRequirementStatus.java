package api.model;

public enum AuditRequirementStatus   {
    PENDING, NA, IN_CONFORMITY, IN_NON_CONFORMITY;
}
