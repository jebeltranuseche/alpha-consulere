package api.model;

import api.adminpayload.BaseEntityDTO;
import api.adminpayload.NonConformityDTO;
import api.model.base_entity.BaseEntity;
import api.model.user.Profile;
import api.model.user.User;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "non_conformities")
public class NonConformity extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Instant nonConformityTime;

    @OneToMany(mappedBy="nonConformity", fetch = FetchType.EAGER)
    private Set<Response> proposedResponses = new HashSet<>();

    @OneToOne(mappedBy = "resolvedNonConformity",cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Response effectiveResponse;

    @NotNull
    @OneToOne(fetch =  FetchType.LAZY)
    @JoinColumn(name = "audit_requirement_id")
    private AuditRequirement auditRequirement;

    @NotNull
    @Enumerated(EnumType.STRING)
    private NonConformityStatus status;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public NonConformityDTO toDTO(boolean withRelations) {
        return new NonConformityDTO(this);
    }

    public Instant getNonConformityTime() {
        return nonConformityTime;
    }

    public Set<Response> getProposedResponses() {
        return proposedResponses;
    }

    public AuditRequirement getAuditRequirement() {
        return auditRequirement;
    }

    public NonConformityStatus getStatus() {
        return status;
    }

    public Response getEffectiveResponse() {
        return effectiveResponse;
    }

    public NonConformity(){

    }
}
