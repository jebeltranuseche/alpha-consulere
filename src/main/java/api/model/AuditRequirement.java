package api.model;

import api.adminpayload.AuditRequirementDTO;
import api.model.base_entity.BaseEntity;
import api.model.user.Profile;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "audit_requirements", uniqueConstraints = {

})
public class AuditRequirement extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToMany(mappedBy = "auditRequirements",fetch = FetchType.LAZY)
    private Set<Process> processes;

    @OneToOne(mappedBy = "auditRequirement",cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private NonConformity nonConformity;
    
    @NotNull
    @Enumerated(EnumType.STRING)
    private AuditRequirementStatus status = AuditRequirementStatus.PENDING;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="audit_id")
    private Audit audit;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="requirement_id")
    private Requirement requirement;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "appointments_audit_requirements",
            joinColumns = @JoinColumn(
                    name = "audit_requirement_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "appointment_id", referencedColumnName = "id"))
    private Set<Appointment> appointments = new HashSet<>();


    public Set<Appointment> getAppointments() {
        return appointments;
    }


    public Audit getAudit() {
        return audit;
    }


    @Override
    public Long getId() {
        return id;
    }

    public Set<Process> getProcesses() {
        return processes;
    }

    public Requirement getRequirement() {
        return requirement;
    }

    @Override
    public AuditRequirementDTO toDTO(boolean withRelations) {
        return new AuditRequirementDTO(this,withRelations);
    }

    public NonConformity getNonConformity() {
        return nonConformity;
    }

    public AuditRequirementStatus getStatus() {
        return status;
    }

    public AuditRequirement(){

    }
}
