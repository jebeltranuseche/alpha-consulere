package api.model;

import api.adminpayload.CommercialEntityDTO;
import api.adminpayload.DepartmentDTO;
import api.exception.AppException;
import api.exception.UserIncongruentRoleBehaviourException;
import api.exception.UserWithoutTierCapacityException;
import api.model.base_entity.BaseEntity;
import api.model.base_entity.CommercialEntity;
import api.model.base_entity.LeadedEntity;
import api.model.tier.BusinessEntity;
import api.model.user.User;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

@Entity
@Table(name = "departments", uniqueConstraints = {
        @UniqueConstraint(columnNames = {
                "company_id",
                "name"
        })
})
public class Department extends CommercialEntity  {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = 30)
    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="leader_id")
    private User leader;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="company_id")
    private Company company;

    @OneToMany(mappedBy="department", fetch = FetchType.EAGER, cascade = {CascadeType.REMOVE},  orphanRemoval = true)
    private Set<Process> processes = new HashSet<>();

    @OneToMany(mappedBy="department", fetch = FetchType.EAGER, cascade = {CascadeType.REMOVE},  orphanRemoval = true)
    private Set<Requirement> requirements = new HashSet<>();

    @OneToMany(mappedBy="department", fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE},  orphanRemoval = true)
    private Set<Audit> audits = new HashSet<>();


    public void setLeader(User leader) {
        this.leader = leader;
    }

    @Override
    public DepartmentDTO toDTO(boolean withRelations) {
        return new DepartmentDTO(this,withRelations);
    }

    public String getName() {
        return name;
    }

    public Company getCompany() {
        return company;
    }

    public Set<Process> getProcesses() {
        return processes;
    }

    @Override
    public User getLeader() {
        return leader;
    }

    @Override
    public Long getId() {
        return id;
    }

    public Set<Audit> getAudits() {
        return audits;
    }

    public Department(@NotBlank @Size(max = 15) String name, User leader, @NotNull Company company) {
        this.name = name;
        this.leader = leader;
        this.company = company;
    }

    public Set<Requirement> getRequirements() {
        return requirements;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean hasActiveAudit(){
        AtomicBoolean activeAudit = new AtomicBoolean(false);
        Iterator<Audit> iterator = audits.iterator();
        while (!activeAudit.get() && iterator.hasNext()) activeAudit.set(iterator.next().isActive());
        return activeAudit.get();
    }

    public Department(){

    }

    @Override
    public User getOwner() {
        return  company.getOwner();
    }

    @Override
    public void setOwner(User user) {
        throw new AppException("Department Owner is derived from its company");
    }
}
