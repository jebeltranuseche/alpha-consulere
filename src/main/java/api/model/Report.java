package api.model;

import api.adminpayload.BaseEntityDTO;
import api.adminpayload.ReportDTO;
import api.model.base_entity.BaseEntity;
import api.model.user.User;

import javax.persistence.*;

@Entity
@Table(name = "reports")
public class Report extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(fetch =  FetchType.LAZY)
    @JoinColumn(name = "audit_id")
    private Audit audit;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public ReportDTO toDTO(boolean withRelations) {
        return null;
    }

    public Report(){

    }
}
