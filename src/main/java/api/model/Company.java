package api.model;

import api.adminpayload.AuditDTO;
import api.adminpayload.CommercialEntityDTO;
import api.adminpayload.CompanyDTO;
import api.exception.UserIncongruentRoleBehaviourException;
import api.exception.UserWithoutTierCapacityException;
import api.model.base_entity.CommercialEntity;
import api.model.tier.BusinessEntity;
import api.model.user.User;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "companies")
public class Company extends CommercialEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = 15)
    private String name;

    @NotBlank
    @Size(max = 128)
    private String bin;

    @Column
    @Lob
    private byte[] picture;

    public Set<Department> getDepartments() {
        return departments;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="owner_id")
    private User owner;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="leader_id")
    private User leader;

    @OneToMany(mappedBy="company", fetch = FetchType.EAGER, cascade = {CascadeType.REMOVE},  orphanRemoval = true)
    private Set<Department> departments = new HashSet<>();

    public Company(@NotBlank @Size(max = 15) String name, @NotBlank @Size(max = 128) String bin, @NotNull User owner, User leader) {
        this.name = name;
        this.bin = bin;
        setOwner(owner);
        setLeader(leader);
    }

    public void setOwner(User owner) {
        if(owner != null && !owner.hasRolesForCreation())
            throw new UserIncongruentRoleBehaviourException(owner.getUsername());
        if(owner != null && !owner.verifyCreationAbility(BusinessEntity.COMPANY))
            throw new UserWithoutTierCapacityException(owner.getUsername());
        this.owner = owner;
    }


    public String getName() {
        return name;
    }

    public String getBin() {
        return bin;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setLeader(User leader) {
        this.leader = leader;
    }

    @Override
    public User getOwner() {
        return owner;
    }

    @Override
    public User getLeader() {
        return leader;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public CompanyDTO toDTO(boolean withRelations){
        return new CompanyDTO(this, withRelations);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBin(String bin) {
        this.bin = bin;
    }

    public Company(){

    }

    public void removeDepartment(Long departmentId){
        departments = departments.stream().filter(department -> department.getId() != departmentId).collect(Collectors.toSet());;
    }
}

