package api.model.tier;



import api.adminpayload.BaseEntityDTO;
import api.adminpayload.TierCapacityDTO;
import api.model.base_entity.BaseEntity;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "tier_capacities", uniqueConstraints = {
        @UniqueConstraint(columnNames = {
                "tier_id",
                "entity"
        })
})

public class TierCapacity extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = 15)
    private String entity;

    @NotNull
    private int capacity;

    public TierCapacity(@NotBlank @Size(max = 15) String entity, @NotNull int capacity, @NotNull Tier tier) {
        this.entity = entity;
        this.capacity = capacity;
        this.tier = tier;
    }

    public TierCapacity() {

    }


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="tier_id")
    private Tier tier;

    public void setEntity(BusinessEntity entity){
        this.entity = entity.getName();
    }

    public String getEntity(){
        return this.entity;
    }

    public int getCapacity(){ return this.capacity; }

    public Long getId(){return this.id;}

    @Override
    public TierCapacityDTO toDTO(boolean withRelations) {
        return new TierCapacityDTO(this);
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }
}
