package api.model.tier;

import api.adminpayload.BaseEntityDTO;
import api.adminpayload.TierCapacityDTO;
import api.adminpayload.TierDTO;
import api.model.base_entity.BaseEntity;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

@Entity
@Table(name = "tiers", uniqueConstraints = {
        @UniqueConstraint(columnNames = {
                "name"
        })
})
public class Tier  extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotBlank
    @Size(max = 15)
    private String name;

    @OneToMany(mappedBy="tier",  cascade = CascadeType.ALL)
    private Set<TierCapacity> tierCapacities = new HashSet<>();

    @OneToMany(mappedBy="tier", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<ContractTier> contracts;
    public Tier(@NotBlank @Size(max = 15) String name) {
        this.name = name;
    }

    public Tier() {

    }

    public Tier(@NotBlank @Size(max = 15) String name, Set<TierCapacity> tierCapacities) {
        this.name = name;
        this.tierCapacities = tierCapacities;
    }



    public Long getId(){
        return  id;
    }

    @Override
    public TierDTO toDTO(boolean withRelations) {
        return new TierDTO(this);
    }

    public String getName() { return name; }

    public int getCreationCapacity(BusinessEntity businessEntity){
        TierCapacity tierCapacity = tierCapacities.stream()
                .filter(element -> element.getEntity().equals(businessEntity.getName()))
                .findAny()
                .orElse(null);
        return tierCapacity != null ? tierCapacity.getCapacity() : 0;
    }

    public Set<TierCapacity> getTierCapacities() {
        return tierCapacities;
    }

    public Set<ContractTier> getContracts() {
        return contracts;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addOrUpdateTierCapacities(Set<TierCapacityDTO> tierCapacitiesDTO) {
        tierCapacitiesDTO.forEach(tierCapacityDTO -> addOrUpdateTierCapacity(tierCapacityDTO));
    }

    public void addOrUpdateTierCapacity(TierCapacityDTO tierCapacityDTO){
        AtomicBoolean updated = new AtomicBoolean(false);
        tierCapacities.forEach(tierCapacity -> {
            if(tierCapacity.getEntity().equals(tierCapacityDTO.getEntity())){
                tierCapacity.setCapacity(tierCapacityDTO.getCapacity());
                updated.set(true);
            }
        });
        if(!updated.get()) tierCapacities.add(new TierCapacity(tierCapacityDTO.getEntity(),tierCapacityDTO.getCapacity(), this));
    }
}
