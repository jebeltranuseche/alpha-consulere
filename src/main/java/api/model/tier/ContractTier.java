package api.model.tier;

import api.adminpayload.BaseEntityDTO;
import api.adminpayload.ContractTierDTO;
import api.exception.BadFormattedContractException;
import api.model.user.User;
import api.model.base_entity.BaseEntity;

import java.time.Instant;

import javax.persistence.*;
import javax.validation.constraints.NotNull;


@Entity
@Table(name = "contract_tiers", uniqueConstraints = {
        @UniqueConstraint(columnNames = {
                "affiliated_id",
                "tier_id"
        })
})
public class ContractTier extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @NotNull
    @ManyToOne()
    @JoinColumn(name="tier_id", updatable = false)
    private Tier tier;


    @NotNull
    @ManyToOne()
    @JoinColumn(name="affiliated_id",  updatable = false)
    private User affiliated;

    @NotNull
    private Instant endDate;

    public Long getId(){ return  id;}

    @Override
    public ContractTierDTO toDTO(boolean withRelations) {
        return new ContractTierDTO(this);
    }

    public Tier getTier() {
        return tier;
    }

    public User getAffiliated() {
        return affiliated;
    }

    public Instant getEndDate() {
        return endDate;
    }

    public void setEndDate(Instant endDate) {
        if(endDate.isBefore(Instant.now())) throw new BadFormattedContractException(affiliated.getUsername());
        this.endDate = endDate;
    }

    private void setAffiliated(User affiliated){
        this.affiliated = affiliated;
    }

    public ContractTier(Tier tier, User affiliated, Instant endDate){
        setAffiliated(affiliated);
        setEndDate(endDate);
        this.tier = tier;
    }

    public ContractTier(Tier tier, User affiliated){
        setAffiliated(affiliated);
        this.tier = tier;
    }

    public ContractTier(){

    }


}
