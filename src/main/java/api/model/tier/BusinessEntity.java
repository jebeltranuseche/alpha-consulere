package api.model.tier;

public enum BusinessEntity {
    USER("users"), COMPANY("companies"), DEPARTMENT("departments"),EVALUATION("evaluations"), AUDITOR_TEAM("auditorTeams"),AUDIT("audits"), PROCESS("processes"),REQUIREMENT("requirements");
    private String name;
    private String ownerGetterMethod;
    private String leaderGetterMethod;
    BusinessEntity(String name) {
        this.name = name;
        this.ownerGetterMethod = "getOwned" + name.substring(0, 1).toUpperCase() + name.substring(1);
        this.leaderGetterMethod = "getLeaded" + name.substring(0, 1).toUpperCase() + name.substring(1);
    }
    public String getName(){
        return name;
    }

    public String getOwnerGetterMethod(){
        return ownerGetterMethod;
    }

    public String getLeaderGetterMethod(){
        return leaderGetterMethod;
    }
}
