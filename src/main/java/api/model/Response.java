package api.model;

import api.adminpayload.BaseEntityDTO;
import api.adminpayload.ResponseDTO;
import api.model.base_entity.BaseEntity;
import api.model.user.Profile;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.Instant;

@Entity
@Table(name = "responses")
public class Response extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Instant responseTime;


    @Size(max = 512)
    private String auditorResponse;

    @NotBlank
    @Size(max = 512)
    private String correctiveAction;

    @NotBlank
    @Size(max = 512)
    private String correction;

    @OneToOne(fetch =  FetchType.LAZY)
    @JoinColumn(name = "resolved_non_conformity_id")
    private NonConformity resolvedNonConformity;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="non_conformity_id")
    private NonConformity nonConformity;

    @NotNull
    @Enumerated(EnumType.STRING)
    private ResponseStatus status;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public ResponseDTO toDTO(boolean withRelations) {
        return new ResponseDTO(this);
    }

    public Instant getResponseTime() {
        return responseTime;
    }

    public String getAuditorResponse() {
        return auditorResponse;
    }

    public String getCorrectiveAction() {
        return correctiveAction;
    }

    public String getCorrection() {
        return correction;
    }

    public NonConformity getResolvedNonConformity() {
        return resolvedNonConformity;
    }

    public NonConformity getNonConformity() {
        return nonConformity;
    }

    public ResponseStatus getStatus() {
        return status;
    }

    public Response(){

    }
}
