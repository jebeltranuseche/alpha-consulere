package api.model;

public enum ResponseStatus {
    PENDING, APPROVED, REJECTED;
}
