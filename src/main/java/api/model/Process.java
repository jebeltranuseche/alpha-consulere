package api.model;

import api.adminpayload.ProcessDTO;
import api.model.base_entity.BaseEntity;
import api.model.base_entity.OwnableEntity;
import api.model.user.User;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

@Entity
@Table(name = "processes", uniqueConstraints = {
        @UniqueConstraint(columnNames = {
                "department_id",
                "name"
        })
})
public class Process extends OwnableEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "processes_requirements",
            joinColumns = @JoinColumn(
                    name = "process_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "requirement_id", referencedColumnName = "id"))
    private Set<Requirement> requirements = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "processes__audit_requirements",
            joinColumns = @JoinColumn(
                    name = "process_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "audit_requirement_id", referencedColumnName = "id"))
    private Set<AuditRequirement> auditRequirements = new HashSet<>();

    public Set<Requirement> getRequirements() {
        return requirements;
    }

    public Department getDepartment() {
        return department;
    }

    @NotBlank
    @Size(max = 128)
    private String name;

    @NotBlank
    @Size(max = 128)
    private String input;

    @NotBlank
    @Size(max = 128)
    private String output;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="department_id")
    private Department department;

    public void setId(Long id) {
        this.id = id;
    }

    public Set<AuditRequirement> getAuditRequirements() {
        return auditRequirements;
    }

    public void setAuditRequirements(Set<AuditRequirement> auditRequirements) {
        this.auditRequirements = auditRequirements;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public ProcessDTO toDTO(boolean withRelations) {
        return new ProcessDTO(this, withRelations);
    }

    public void setRequirements(Set<Requirement> requirementsToAdd,  Set<Requirement>requirementsToRemove) {
        if(requirements == null || requirements.size() == 0){
            requirements = new HashSet<>();
            requirementsToRemove = null;
        }
        requirements.addAll(requirementsToAdd);
        requirements.removeAll(requirementsToRemove);
    }

    public void addRequirements(Set<Requirement> requirementsToAdd){
        long departmentId = department.getId();
        requirementsToAdd.forEach(requirement -> {if(requirement.getDepartment().getId() == departmentId) requirements.add(requirement);});
    }

    public void removeRequirements(Set<Requirement> requirementsToRemove){
        if(requirements.size() != 0) requirements.removeAll(requirementsToRemove);
    }

    public Process(@NotBlank @Size(max = 128) String name, @NotBlank @Size(max = 128) String input, @NotBlank @Size(max = 128) String output, @NotNull Department department) {
        this.name = name;
        this.input = input;
        this.output = output;
        this.department = department;
    }

    public boolean hasActiveAudit(){
        AtomicBoolean activeAudit = new AtomicBoolean(false);
        Iterator<AuditRequirement> iterator = auditRequirements.iterator();
        while (!activeAudit.get() && iterator.hasNext()) activeAudit.set(iterator.next().getAudit().isActive());
        return activeAudit.get();
    }

    public Process(){

    }

    @Override
    public User getOwner() {
        return department.getCompany().getOwner();
    }

    @Override
    public void setOwner(User user) {

    }
}
