package api.model;

import api.adminpayload.BaseEntityDTO;
import api.adminpayload.RequirementDTO;
import api.exception.BadRequestException;
import api.exception.UserIncongruentRoleBehaviourException;
import api.exception.UserWithoutTierCapacityException;
import api.model.base_entity.BaseEntity;
import api.model.base_entity.OwnableEntity;
import api.model.tier.BusinessEntity;
import api.model.user.User;
import api.model.Process;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

@Entity
@Table(name = "requirements", uniqueConstraints = {
@UniqueConstraint(columnNames = {
        "department_id",
        "name"
})})
public class Requirement  extends OwnableEntity  {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = 15)
    private String name;

    @NotBlank
    @Size(max = 256)
    private String description;

    @ManyToMany(mappedBy = "requirements",fetch = FetchType.LAZY)
    private Set<Process> processes = new HashSet<>();

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="department_id")
    private Department department;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="filter_requirement_id")
    private Requirement filterRequirement;

    @OneToMany(mappedBy="filterRequirement", fetch = FetchType.LAZY)
    private Set<Requirement> filteredRequirements = new HashSet<>();

    @OneToMany(mappedBy="requirement", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<AuditRequirement> auditRequirements = new HashSet<>();


    public Set<AuditRequirement> getAuditRequirements() {
        return auditRequirements;
    }



    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setFilterRequirement(Requirement filterRequirement) {
        this.filterRequirement = filterRequirement;
    }


    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Set<Process> getProcesses() {
        return processes;
    }

    public Requirement getFilterRequirement() {
        return filterRequirement;
    }

    public Set<Requirement> getFilteredRequirements() {
        return filteredRequirements;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public RequirementDTO toDTO(boolean withRelations) {
        return new RequirementDTO(this, withRelations);
    }

    public Department getDepartment() {
        return department;
    }

    public void addProcesses(Set<Process> processesToAdd){
        long departmentId = department.getId();
        processesToAdd.forEach(process -> {if(departmentId == process.getDepartment().getId()) processes.add(process);});
    }

    public void removeProcesses(Set<Process> processesToRemove){
        if(processes.size() != 0) processes.removeAll(processesToRemove);
    }

    public boolean hasActiveAudit(){
        AtomicBoolean activeAudit = new AtomicBoolean(false);
        Iterator<AuditRequirement> iterator = auditRequirements.iterator();
        while (!activeAudit.get() && iterator.hasNext()) activeAudit.set(iterator.next().getAudit().isActive());
        return activeAudit.get();
    }

    public Requirement(@NotBlank @Size(max = 15) String name, @NotBlank @Size(max = 256) String description, Requirement filterRequirement, @NotNull Department department) {
        this.name = name;
        this.description = description;
        if(filterRequirement != null && filterRequirement.getDepartment().getId() != department.getId()) throw new BadRequestException("filter requirement must have the same department as the created filter");
        this.filterRequirement = filterRequirement;
        this.department = department;
    }

    public Requirement(){

    }

    @Override
    public User getOwner() {
        return department.getCompany().getOwner();
    }

    @Override
    public void setOwner(User user) {

    }
}
