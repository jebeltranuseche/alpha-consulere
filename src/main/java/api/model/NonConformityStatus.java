package api.model;

public enum NonConformityStatus {
    OPEN, PROCESSING ,CLOSED;
}
