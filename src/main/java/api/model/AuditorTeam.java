package api.model;

import api.adminpayload.AuditorTeamDTO;
import api.adminpayload.CommercialEntityDTO;
import api.model.base_entity.CommercialEntity;
import api.model.user.User;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "auditor_teams")
public class AuditorTeam extends CommercialEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany(mappedBy="auditorTeam", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<Audit> audits = new HashSet<>();

    @Column
    @Lob
    private byte[] picture;

    @NotBlank
    @Size(max = 15)
    private String name;

    @NotBlank
    @Size(max = 15)
    private String bin;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="owner_id")
    private User owner;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="leader_id")
    private User leader;

    @ManyToMany(mappedBy = "auditorTeams",fetch = FetchType.LAZY)
    private Set<User> auditors = new HashSet<>();

    @Override
    public void setOwner(User owner) {
        this.owner = owner;
    }

    public void setLeader(User leader) {
        this.leader = leader;
    }

    @Override
    public AuditorTeamDTO toDTO(boolean withRelations) {
        return new AuditorTeamDTO(this, withRelations);
    }

    @Override
    public User getOwner() {
        return owner;
    }

    @Override
    public User getLeader() {
        return leader;
    }

    @Override
    public Long getId() {
        return id;
    }

    public Set<Audit> getAudits() {
        return audits;
    }

    public byte[] getPicture() {
        return picture;
    }

    public String getName() {
        return name;
    }

    public String getBin() {
        return bin;
    }

    public Set<User> getAuditors() {
        return auditors;
    }

    public AuditorTeam(@NotBlank @Size(max = 15) String name, @NotBlank @Size(max = 15) String bin, @NotNull User owner, User leader) {
        this.name = name;
        this.bin = bin;
        this.owner = owner;
        this.leader = leader;
    }

    public void addAuditors(Set<User> auditorsToAdd){
        auditors.addAll(auditorsToAdd);
    }

    public void removeRequirements(Set<User> auditorsToRemove){
        if(auditors != null && auditors.size() != 0) auditors.removeAll(auditorsToRemove);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBin(String bin) {
        this.bin = bin;
    }

    public AuditorTeam(){

    }
}
