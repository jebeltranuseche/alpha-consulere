package api.model.user;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity

@Table(name = "profiles", uniqueConstraints = {
        @UniqueConstraint(columnNames = {
                "user_id"
        })
})
public class Profile {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = 15)
    private String firstName;

    @NotBlank
    @Size(max = 15)
    private String lastName;

    @NotBlank
    @Size(max = 25)
    private String dni;

    @Column
    @Lob
    private byte[] picture;

    @OneToOne(fetch =  FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;


    public Profile(@NotBlank @Size(max = 15) String firstName, @NotBlank @Size(max = 15) String lastName, @NotBlank @Size(max = 25) String dni) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dni = dni;
    }

    public Profile(){

    }

    public Profile(User user){
        this.user = user;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getDni() {
        return dni;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public void setProfile(Profile profile){
        dni = profile.getDni();
        firstName = profile.getFirstName();
        lastName = profile.getLastName();
    }
}
