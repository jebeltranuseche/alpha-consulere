package api.model.user;

import api.adminpayload.ProfileDTO;
import api.adminpayload.UserDTO;
import api.exception.*;
import api.model.*;
import api.model.Process;
import api.model.base_entity.CommercialEntity;
import api.model.base_entity.LeadedEntity;
import api.model.base_entity.OwnableEntity;
import api.model.role_based_access.Role;
import api.model.tier.BusinessEntity;
import api.model.tier.ContractTier;
import api.model.tier.Tier;
import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.Instant;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import static java.util.stream.Collectors.toSet;

@Entity
@Table(name = "users", uniqueConstraints = {
        @UniqueConstraint(columnNames = {
                "username"
        }),
        @UniqueConstraint(columnNames = {
                "email"
        })
})
public class User extends OwnableEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = 15)
    private String username;

    @NotBlank
    @Size(max = 40)
    @Email
    private String email;

    @NotBlank
    @Size(max = 100)
    private String password;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinTable(
            name = "roles_users",
            joinColumns = @JoinColumn(
                    name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "role_id", referencedColumnName = "id"))
    private Set<Role> roles = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "users_auditor_teams",
            joinColumns = @JoinColumn(
                    name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "auditor_team_id", referencedColumnName = "id"))
    private Set<AuditorTeam> auditorTeams = new HashSet<>();

    @OneToOne(mappedBy = "user",cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Profile profile;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="owner_id")
    private User owner;

    @OneToMany(mappedBy="affiliated", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<ContractTier> contracts = new HashSet<>();

    @OneToMany(mappedBy="owner", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    private Set<User> users = new HashSet<>();

    @OneToMany(mappedBy="owner", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    private Set<Audit> ownedAudits  = new HashSet<>();

    @OneToMany(mappedBy="leader", fetch = FetchType.LAZY)
    private Set<Audit> leadedAudits  = new HashSet<>();

    @OneToMany(mappedBy="owner", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    private Set<Company> ownedCompanies  = new HashSet<>();

    @OneToMany(mappedBy="leader", fetch = FetchType.LAZY)
    private Set<Company> leadedCompanies  = new HashSet<>();

    @OneToMany(mappedBy="owner", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    private Set<AuditorTeam> ownedAuditorTeams  = new HashSet<>();

    @OneToMany(mappedBy="leader", fetch = FetchType.LAZY)
    private Set<AuditorTeam> leadedAuditorTeams  = new HashSet<>();

    @OneToMany(mappedBy="leader", fetch = FetchType.LAZY)
    private Set<Department> leadedDepartments  = new HashSet<>();

    @OneToMany(mappedBy="owner", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    private Set<Evaluation> ownedEvaluations  = new HashSet<>();

    @OneToMany(mappedBy="leader", fetch = FetchType.LAZY)
    private Set<Evaluation> leadedEvaluations  = new HashSet<>();

    public User(@NotBlank @Size(max = 15) String username, @NotBlank @Size(max = 40) @Email String email, @NotBlank @Size(max = 100) String password) {
        this.username = username;
        this.email = email;
        this.password = password;
    }

    public User() {

    }

    public Set<User> getOwnedUsers() {
        return users;
    }

    public Long getId() {
        return id;
    }

    @Override
    public UserDTO toDTO(boolean withRelations) {
        return new UserDTO(this,withRelations);
    }

    public User getOwner() {
        return owner;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Tier getTier(){
        return getActiveContractOrException().getTier();
    }

    public Tier getTierOrException(){
        Tier tier = getTier();
        if(tier == null) throw new UserWithoutTierException(username);
        return tier;
    }

    public Set<Requirement> getOwnedRequirements(){
        Set<Requirement> requirements = new HashSet<>();
        ownedCompanies.forEach(company -> company.getDepartments().forEach(department -> requirements.addAll(department.getRequirements()) ));
        return requirements;
    }

    public Set<Process> getOwnedProcesses(){
        Set<Process> processes = new HashSet<>();
        ownedCompanies.forEach(company -> company.getDepartments().forEach(department -> processes.addAll(department.getProcesses()) ));
        return processes;
    }

    public boolean hasActiveContract(){
        return contracts.stream().
                anyMatch(contractTier -> contractTier.getEndDate().isAfter(Instant.now()));
    }

    public Optional<ContractTier> getActiveContract(){
            Optional<ContractTier> optionalContract = contracts.stream().
                filter(contractTier -> contractTier.getEndDate().isAfter(Instant.now())).
                findFirst();
            return optionalContract != null? optionalContract: Optional.empty();
    }

    public void expireActiveContract(){
        contracts =  contracts.stream()
                .map(contractTier ->{
                    if(contractTier.getEndDate().isAfter(Instant.now()) ) contractTier.setEndDate(Instant.now());
                    return contractTier;
                }).collect(toSet());
    }

    public ContractTier getActiveContractOrException(){
        return getActiveContract().orElseThrow(() -> new UserWithoutActiveContractException(username));
    }

    public boolean hasRole(String roleName){
        AtomicBoolean hasRole = new AtomicBoolean(false);
        getRoles().forEach((role) -> {
            if(role.getName().equals(roleName)) hasRole.set(true);
        });
        return hasRole.get();
    }

    public void addRole(Role role){
        roles.add(role);
    }

    public void setUsers(Set<User> users) {
        if(!verifyCreationAbilityFor(BusinessEntity.USER, users.size()))
            throw new UserWithoutTierCapacityException(getUsername());
        users.forEach((u)-> {
            System.out.println(u.getUsername());if(u.hasRolesForCreation()) throw new UserIncongruentRoleBehaviourException(u.getUsername());});
        System.out.println(users.size());
        this.users.addAll(users);
    };

    public void setOwner(User owner) {
        if(owner != null && hasRolesForCreation())
            throw new UserIncongruentRoleBehaviourException(username);
        if(owner != null && !owner.verifyCreationAbility(BusinessEntity.USER))
            throw new UserWithoutTierCapacityException(owner.getUsername());
        this.owner = owner;
    }

    public Set<ContractTier> getContracts() {
        return contracts;
    }

    public Set<Long> getContractsIds(){
        Set<Long> contractsIds = new HashSet<Long>();
        contracts.forEach(contractTier ->  contractsIds.add(contractTier.getId()));
        return contractsIds;
    }

    public Set<Long> getUserIds(){
        Set<Long> usersIds = new HashSet<Long>();
        users.forEach(user ->  usersIds.add(user.getId()));
        return usersIds;
    }

    public Set<OwnableEntity> getChildren(BusinessEntity businessEntity){
        Set<OwnableEntity> children = new HashSet<OwnableEntity>();
        try {
           children = (Set<OwnableEntity>) getClass().getMethod(businessEntity.getOwnerGetterMethod()).invoke(this);
        } catch (ReflectiveOperationException e) {
            throw new AppException("Business entities definition error: " + businessEntity);
        }
        return children;
    }

    public Set<CommercialEntity> getLeadedEntities(BusinessEntity businessEntity){
        Set<CommercialEntity> leadedEntities = new HashSet<CommercialEntity>();
        try {
            leadedEntities = (Set<CommercialEntity>) getClass().getMethod(businessEntity.getLeaderGetterMethod()).invoke(this);
        } catch (ReflectiveOperationException e) {
            throw new AppException("Business entities definition error: ");
        }
        return leadedEntities;
    }


    public int getActualCapacity(BusinessEntity businessEntity) {
        int capacity = 0;
        User creationUser = hasRolesForCreation()? this : owner;
        if(creationUser.hasActiveContract()){
            int tierCapacity = creationUser.getTierOrException().getCreationCapacity(businessEntity);
            int inUseCapacity= creationUser.getInUseCapacity(businessEntity);
            capacity = tierCapacity - inUseCapacity;
        }
        return capacity;
    }

    public int getTierCapacity(BusinessEntity businessEntity){
       return getTierOrException().getCreationCapacity(businessEntity);
    }

    public boolean hasCapacity(BusinessEntity businessEntity) {
        int tierCapacity =  getTierOrException().getCreationCapacity(businessEntity);
        int inUseCapacity= getInUseCapacity(businessEntity);
        return tierCapacity > inUseCapacity;
    }

    public int getInUseCapacity(BusinessEntity businessEntity){
        return getChildren(businessEntity).size();
    }

    public boolean hasRolesForCreation(){
        return hasRole("ROLE_ADMIN") || (hasRole("ROLE_CLIENT") && hasActiveContract());
    }

    public boolean verifyCreationAbility(BusinessEntity businessEntity){
        return hasRole("ROLE_ADMIN") || (hasRole("ROLE_CLIENT") && hasCapacity(businessEntity));
    }
    public boolean verifyCreationAbilityFor(BusinessEntity businessEntity,int numberOfEntities){
        return hasRole("ROLE_ADMIN") || (hasRole("ROLE_CLIENT") && getTierCapacity(businessEntity) >= numberOfEntities);
    }

    public boolean verifyDelegatedCreationAbility(BusinessEntity businessEntity){
        return  hasRole("ROLE_ADMIN") || (owner == null && verifyCreationAbility(businessEntity) || (owner !=null && owner.verifyCreationAbility(businessEntity))) ;
    }

    public Optional<ContractTier> getContractForTier(Tier tier){
        AtomicReference<Optional<ContractTier>> contractTier = new AtomicReference<>(Optional.empty());
        contracts.forEach(contract -> {if(contract.getTier().getId() == tier.getId()) contractTier.set(Optional.of(contract));});
        return contractTier.get() != null? contractTier.get() : Optional.empty() ;
    }

    public void  modifyOrCreateContractForTier(ContractTier contractTier){
        AtomicReference<Boolean> modified = new AtomicReference<Boolean>(false)  ;
        contracts.forEach(contract -> {
            if(contract.getTier().getId() == contractTier.getTier().getId()) {
                contract.setEndDate(contractTier.getEndDate());
                modified.set(true);
            }
        });
        if(!modified.get()) contracts.add(contractTier);
    }

    public void addOrRenewContract(ContractTier contractTier){
        expireActiveContract();
        modifyOrCreateContractForTier(contractTier);
    }

    public boolean verifyResponsibilityOverDepartment(Department department, boolean absolutePermission ){
        System.out.println(verifyResponsibilityOfCommercialEntity(department.getCompany()));
       return hasRole("ROLE_ADMIN") || verifyResponsibilityOverCompany(department.getCompany(), false) || (!absolutePermission && verifyLeaderShipOfCommercialEntity(department) );
    }

    public boolean verifyResponsibilityOverCompany(Company company, boolean absolutePermission ){
        return hasRole("ROLE_ADMIN") || verifyOwnershipOfCommercialEntity(company) || (!absolutePermission && verifyLeaderShipOfCommercialEntity(company) );
    }

    public boolean verifyAbsolutePermissionOverCommercialEntity(CommercialEntity commercialEntity){
        return  hasRole("ROLE_ADMIN") || verifyOwnershipOfCommercialEntity(commercialEntity);
    }

    public boolean verifyBasicPermissionOverCommercialEntity(CommercialEntity commercialEntity){
        return  hasRole("ROLE_ADMIN") || verifyOwnershipOfCommercialEntity(commercialEntity) || verifyLeaderShipOfCommercialEntity(commercialEntity);
    }

    public boolean verifyPermissionOverCommercialEntity(CommercialEntity commercialEntity, boolean onlyRead, boolean absolutePermission){
        if(onlyRead) return hasPermissionToSeeCommercialEntity(commercialEntity);
        return absolutePermission? verifyAbsolutePermissionOverCommercialEntity(commercialEntity) : verifyBasicPermissionOverCommercialEntity(commercialEntity);
    }

    public boolean verifyPermissionOverOwnableEntity(OwnableEntity ownableEntity, boolean onlyRead){
        if(onlyRead) return hasPermissionToSeeOwnableEntity(ownableEntity);
        return hasPermissionOverOwnableEntity(ownableEntity);
    }

    public boolean hasAbsolutePermissionOverCommercialEntity(CommercialEntity commercialEntity){
        return  verifyOwnershipOfCommercialEntity(commercialEntity);
    }

    public boolean hasBasicPermissionOverCommercialEntity(CommercialEntity commercialEntity){
        return verifyOwnershipOfCommercialEntity(commercialEntity) || verifyLeaderShipOfCommercialEntity(commercialEntity);
    }

    public boolean hasPermissionToSeeOwnableEntity(OwnableEntity ownableEntity){
        return hasRole("ROLE_ADMIN") || hasPermissionOverOwnableEntity(ownableEntity) || (owner != null && owner.hasPermissionOverOwnableEntity(ownableEntity));
    }

    public boolean hasPermissionToSeeCommercialEntity(CommercialEntity commercialEntity){
        return hasBasicPermissionOverCommercialEntity(commercialEntity) || (owner != null && owner.hasAbsolutePermissionOverCommercialEntity(commercialEntity));
    }

    public boolean hasPermissionOverOwnableEntity(OwnableEntity ownableEntity){
        return hasRole("ROLE_ADMIN") || verifyOwnershipOfCommercialEntity(ownableEntity);
    }

    public boolean verifyResponsibilityOfCommercialEntity(CommercialEntity commercialEntity){
        return verifyOwnershipOfCommercialEntity(commercialEntity) || verifyLeaderShipOfCommercialEntity(commercialEntity);
    }


    public boolean verifyOwnershipOfCommercialEntity(OwnableEntity ownableEntity){
        User owner = ownableEntity.getOwner();
        return  owner != null && id == owner.getId() && isClientActive();
    }

    public boolean verifyLeaderShipOfCommercialEntity(CommercialEntity commercialEntity){
        return commercialEntity.getOwner() != null  && commercialEntity.getLeader() != null && id == commercialEntity.getLeader().getId() && isUserActive();
    }

    public boolean isClientActive(){
        return hasRole("ROLE_CLIENT") && hasActiveContract() && profile != null;
    }

    public boolean isUserActive(){
        return profile != null && (isClientActive() || (owner != null && owner.isUserActive()));
    }

    public Profile getProfile() {
        return profile;
    }

    public Set<Audit> getOwnedAudits() {
        return ownedAudits;
    }

    public Set<Audit> getLeadedAudits() {
        return leadedAudits;
    }

    public Set<Company> getOwnedCompanies() {
        return ownedCompanies;
    }

    public Set<Company> getLeadedCompanies() {
        return leadedCompanies;
    }

    public Set<AuditorTeam> getOwnedAuditorTeams() {
        return ownedAuditorTeams;
    }

    public Set<AuditorTeam> getLeadedAuditorTeams() {
        return leadedAuditorTeams;
    }

    public Set<Department> getLeadedDepartments() {
        return leadedDepartments;
    }

    public Set<Department> getOwnedDepartments() {
        Set<Department> ownedDepartments = new HashSet<>();
        ownedCompanies.forEach(company -> ownedDepartments.addAll(company.getDepartments()));
        return ownedDepartments;
    }

    public Set<Evaluation> getOwnedEvaluations() {
        return ownedEvaluations;
    }

    public Set<Evaluation> getLeadedEvaluations() {
        return leadedEvaluations;
    }

    public void setProfile(ProfileDTO profileDTO) {
        if(profile == null) profile = new Profile(this);
        if(profileDTO.getIdentificationDocument() != null) profile.setDni(profileDTO.getIdentificationDocument());
        if(profileDTO.getFirstName() != null) profile.setFirstName(profileDTO.getFirstName());
        if(profileDTO.getLastName() != null) profile.setLastName(profileDTO.getLastName());
        System.out.println(profile != null);
    }
}