package api.model.role_based_access;

public enum  RoleName {
    ROLE_USER,
    ROLE_AUDITOR,
    ROLE_CLIENT,
    ROLE_ADMIN,
}