package api.model.role_based_access;

import api.adminpayload.BaseEntityDTO;
import api.adminpayload.RoleDTO;
import api.model.user.User;
import api.model.base_entity.BaseEntity;
import javax.persistence.*;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "roles")
public class Role extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 15)
    private String name;

    @Column(length = 256)
    private String description;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "permissions_roles",
            joinColumns = @JoinColumn(
                    name = "role_id"),
            inverseJoinColumns = @JoinColumn(
                    name = "permission_id"))
    private Set<Permission> permissions;

    @ManyToMany(mappedBy = "roles",fetch = FetchType.LAZY)
    private Set<User> users;

    public Role() {

    }

    public Role(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    @Override
    public RoleDTO toDTO(boolean withRelations) {
        return new RoleDTO(this);
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public Set<Permission> getPermissions() {
        return permissions;
    }

    public Set<String> getPermissionsNames(){
        Set<String> permissionsNames = permissions.stream().map(
                permission -> permission.getName()
        ).collect(Collectors.toSet());
        return permissionsNames;
    }

    public Set<String> getAuthoritiesNames(){
        Set<String> authoritiesNames = getPermissionsNames() ;
        authoritiesNames.add(name);
        return authoritiesNames;
    }
    public void setPermissions(Set<Permission> permissions) {
        this.permissions = permissions;
    }
}