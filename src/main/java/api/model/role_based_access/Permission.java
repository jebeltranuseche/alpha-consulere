package api.model.role_based_access;

import api.adminpayload.BaseEntityDTO;
import api.model.base_entity.BaseEntity;
import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "permissions")
public class Permission extends BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 15)
    private String name;

    @Column(length = 256)
    private String description;

    @ManyToMany(mappedBy = "permissions",fetch = FetchType.LAZY)
    private Set<Role> roles;

    public Permission() {

    }

    public Permission(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    @Override
    public BaseEntityDTO toDTO(boolean withRelations) {
        return null;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
