package api.service;

import api.adminpayload.DepartmentDTO;
import api.adminpayload.DepartmentsDTO;
import api.adminpayload.RequirementDTO;
import api.adminpayload.RequirementsDTO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

@Service
public interface RequirementService {

    public RequirementsDTO getRequirements();

    public RequirementDTO createRequirement(RequirementDTO requirementDTO);

    @PreAuthorize("hasRole('CLIENT') or hasRole('ROLE_ADMIN')")
    public RequirementDTO deleteRequirement(long requirementId);

    public RequirementDTO patchRequirement(long requirementId, RequirementDTO requirementDTO);

    public RequirementDTO getRequirement(long requirementId);

    public RequirementsDTO getVisibleRequirements();
}
