package api.service;
import api.adminpayload.*;
import api.exception.*;
import api.model.user.User;
import api.model.role_based_access.Role;
import api.model.tier.BusinessEntity;
import api.model.tier.ContractTier;
import api.model.tier.Tier;
import api.payload.ContractTierRequest;
import api.payload.CreateUserDTO;
import api.payload.SoftPatchUserDTO;
import api.repository.RoleRepository;
import api.repository.TierRepository;
import api.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    AuthServiceImpl authService;
    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    TierRepository tierRepository;
    @Autowired
    PasswordEncoder passwordEncoder;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public UsersDTO getUsers(){
        return new UsersDTO(userRepository.findAll());
    }

    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public UserDTO createUser(@Valid @RequestBody CreateUserDTO createUserDTO) {
        validateUserAvailability(createUserDTO.getUsername(), createUserDTO.getEmail());
        User user = new User(createUserDTO.getUsername(), createUserDTO.getEmail(), passwordEncoder.encode(createUserDTO.getPassword()));
        long[] ids = createUserDTO.getRolesIds();
        if (ids != null) user.setRoles(roleRepository.findByIdIn(ids));
        if (createUserDTO.getOwnerId() != 0)
            user.setOwner(userRepository.findById(createUserDTO.getOwnerId()).orElseThrow(() -> new EntityNotFoundException("owner")));
        if (createUserDTO.getContractTierRequest() != null)
            addContractTierToAnUser(user, createUserDTO.getContractTierRequest());
        if(createUserDTO.getProfile() != null)
            user.setProfile(createUserDTO.getProfile());
        userRepository.save(user);
        return user.toDTO(true);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @Transactional
    public ContractTierDTO affiliateUser(long userId, @Valid @RequestBody ContractTierRequest contractTierRequest) {
        User user = userRepository.findById(userId).orElseThrow(() -> new EntityNotFoundException("owner"));
        return new ContractTierDTO(addContractTierToAnUser(user, contractTierRequest));
    }


    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @Transactional
    private ContractTier addContractTierToAnUser(User user, ContractTierRequest contractTierRequest) {
        Tier tier = tierRepository.findById(contractTierRequest.getTierId()).orElseThrow(() -> new EntityNotFoundException("tier"));
        ContractTier contractTier = new ContractTier(tier, user, contractTierRequest.getContractEnding().toInstant());
        user.addOrRenewContract(contractTier);
        upgradeUserToClient(user);
        return contractTier;
    }


    public UsersDTO getOwnedUsers() {
        User user = getSessionUser();
        long sessionId = user.getId();
        if(!user.hasRolesForCreation() && user.getOwner()!=null) user = user.getOwner();
        UsersDTO usersResponse = new UsersDTO(user.getOwnedUsers());
        return usersResponse;
    }


    public UserDTO getUser(long userId) {
        User sessionUser = getSessionUser();
        User user = userRepository.findById(userId).orElseThrow(() -> new EntityNotFoundException("user"));
        if(sessionUser.getId()!= user.getId() && !sessionUser.hasPermissionOverOwnableEntity(user)) throw new UnauthorizedException(sessionUser.getUsername());
        return user.toDTO(true);
    }

    public UserDTO getSession() {
        User sessionUser = getSessionUser();
        return sessionUser.toDTO(true);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    private User assignRoleToUser(User user, String roleName) {
        Role role = roleRepository.findByName(roleName).orElseThrow(() -> new EntityNotFoundException("role"));
        user.addRole(role);
        return user;
    }

    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    private User upgradeUserToClient(User user) {
        if (!user.hasRole("ROLE_CLIENT")) user = assignRoleToUser(user, "ROLE_CLIENT");
        user.setOwner(null);
        userRepository.save(user);
        return user;
    }

    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_CLIENT')")
    public UserDTO deleteUser(long userId) {
        User sessionUser = getSessionUser();
        User userToDelete = userRepository.findById(userId).orElseThrow(() -> new EntityNotFoundException("user to delete"));
        if ( sessionUser.getId()!= userToDelete.getId() && !sessionUser.hasPermissionOverOwnableEntity(userToDelete))
            throw new UnauthorizedException("Session user");
        userRepository.delete(userToDelete);
        return userToDelete.toDTO(true);
    }

    @Transactional
    public UserDTO patchUser(long userId, @Valid @RequestBody SoftPatchUserDTO softPatchUserDTO) {
        User sessionUser = getSessionUser();
        User user = userRepository.findById(userId).orElseThrow(() -> new EntityNotFoundException("user"));
        if (sessionUser.getId()!= user.getId() && !sessionUser.hasPermissionOverOwnableEntity(user))
            throw new UnauthorizedException(sessionUser.getUsername());
        if (softPatchUserDTO.getUsername() != null && validateUsernameAvailability(softPatchUserDTO.getUsername()))
            user.setUsername(softPatchUserDTO.getUsername());
        if (softPatchUserDTO.getEmail() != null && validateEmailAvailability(softPatchUserDTO.getEmail()))
            user.setEmail(softPatchUserDTO.getEmail());
        if (softPatchUserDTO.getPassword() != null) {
            if (!sessionUser.hasPermissionOverOwnableEntity(user) && (softPatchUserDTO.getOldPassword() == null || !passwordEncoder.matches(softPatchUserDTO.getOldPassword(), user.getPassword())))
                throw new BadRequestException("Invalid Old Password");
            user.setPassword(passwordEncoder.encode(softPatchUserDTO.getPassword()));
        }
        if (softPatchUserDTO.getProfile() != null) user.setProfile(softPatchUserDTO.getProfile());
        UserDTO resultDTO =userRepository.save(user).toDTO(true);
        return resultDTO;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_CLIENT')")
    public UserCapacityDTO getSessionUserActualCapacityFor(String entity){
         User sessionUser = getSessionUser();
        return new UserCapacityDTO(sessionUser.getUsername(), sessionUser.getActualCapacity(BusinessEntity.valueOf(entity))) ;
    }

    public Set<UserCapacityDTO> loadAllActualCapacities(){
        User sessionUser = getSessionUser();
        BusinessEntity[]  entities = BusinessEntity.values();
        Set<UserCapacityDTO> capacities = new HashSet<>();
        for (int i = 0; i <  entities.length ; i++) {
            capacities.add(new UserCapacityDTO(entities[i].getName(),sessionUser.getActualCapacity(entities[i])));
        }
        return capacities ;
    }

    public boolean validateUserAvailability(String username, String email) {
        return validateUsernameAvailability(username) && validateEmailAvailability(email);
    }

    public boolean validateUsernameAvailability(String username) {
        if (userRepository.existsByUsername(username))
            throw new UsernameAlreadyInUseException(username);
        return true;
    }

    public boolean validateEmailAvailability(String email) {
        if (userRepository.existsByEmail(email))
            throw new UserWithEmailAlreadyExistsException(email);
        return true;
    }

    public User getSessionUser(){
        return authService.getSessionUser("Session user");
    }
}
