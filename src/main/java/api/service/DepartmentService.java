package api.service;

import api.adminpayload.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

@Service
public interface DepartmentService {
    public DepartmentsDTO getDepartments();

    @PreAuthorize("hasRole('ROLE_CLIENT') or hasRole('ROLE_ADMIN')")
    public DepartmentDTO createDepartment(DepartmentDTO createDepartmentDTO);

    @PreAuthorize("hasRole('CLIENT') or hasRole('ROLE_ADMIN')")
    public DepartmentDTO deleteDepartment(long id);

    public DepartmentDTO patchDepartment(long departmentId, DepartmentDTO departmentDTO);

    public DepartmentDTO getDepartment(long departmentId);

    public DepartmentsDTO getVisibleDepartments();
}
