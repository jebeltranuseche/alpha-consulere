package api.service;

import api.adminpayload.AuditorTeamDTO;
import api.adminpayload.AuditorTeamsDTO;
import api.exception.EntityNotFoundException;
import api.exception.UnauthorizedException;
import api.exception.UserWithoutTierCapacityException;
import api.model.AuditorTeam;
import api.model.tier.BusinessEntity;
import api.model.user.User;
import api.repository.AuditorTeamRepository;
import api.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AuditorTeamServiceImpl  implements  AuditorTeamService{
    @Autowired
    UserService userService;
    @Autowired
    AuditorTeamRepository auditorTeamRepository;

    @Autowired
    UserRepository userRepository;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public AuditorTeamsDTO getAuditorTeams(){
        return new AuditorTeamsDTO(auditorTeamRepository.findAll(), true);
    }

    @Transactional
    @PreAuthorize("hasRole('ROLE_CLIENT') or hasRole('ROLE_ADMIN')")
    public AuditorTeamDTO createAuditorTeam(AuditorTeamDTO auditorTeamDTO){
        User sessionUser = userService.getSessionUser();
        if(!sessionUser.verifyCreationAbility(BusinessEntity.AUDITOR_TEAM)) throw new UserWithoutTierCapacityException(sessionUser.getUsername());
        User owner = (sessionUser.hasRole("ROLE_ADMIN") && auditorTeamDTO.getOwnerId() != 0)?  userRepository.findById(auditorTeamDTO.getOwnerId()).orElseThrow(()-> new EntityNotFoundException("owner")) : sessionUser;
        User leader = userRepository.findById(auditorTeamDTO.getLeaderId()).orElse(null);
        AuditorTeam auditorTeam = new AuditorTeam(auditorTeamDTO.getName(),auditorTeamDTO.getName(),owner, leader);
        if(auditorTeamDTO.getAuditorsToAdd()!=null && auditorTeamDTO.getAuditorsToAdd().length !=0) auditorTeam.addAuditors(userRepository.findAllById(auditorTeamDTO.getAuditorsToAdd()));
        auditorTeamRepository.save(auditorTeam);
        return  auditorTeam.toDTO(true);
    }

    @Transactional
    @PreAuthorize("hasRole('CLIENT') or hasRole('ROLE_ADMIN')")
    public AuditorTeamDTO deleteAuditorTeam(long auditorTeamId){
        AuditorTeam auditorTeam =  searchAuditorTeam(auditorTeamId,true,false);
        auditorTeamRepository.delete(auditorTeam);
        return auditorTeam.toDTO(true);
    }

    @Transactional
    public AuditorTeamDTO patchAuditorTeam(long auditorTeamId, AuditorTeamDTO auditorTeamDTO){
        AuditorTeam auditorTeam = searchAuditorTeam(auditorTeamId, false,false);
        if(auditorTeamDTO.getName() != null && !auditorTeamDTO.getName().equals("")) auditorTeam.setName(auditorTeamDTO.getName());
        if(auditorTeamDTO.getBin() != null && !auditorTeamDTO.getBin().equals("")) auditorTeam.setBin(auditorTeamDTO.getName());
        if(auditorTeamDTO.getLeaderId() != 0){
            User leader = userRepository.findById(auditorTeamDTO.getLeaderId()).orElseThrow(()-> new EntityNotFoundException("leader"));
            auditorTeam.setLeader(leader);
        }
        auditorTeamRepository.save(auditorTeam);
        return auditorTeam.toDTO(true);
    }

    private AuditorTeam searchAuditorTeam(long auditorTeamId,  boolean absolutePermission, boolean onlyRead){
        User sessionUser = userService.getSessionUser();
        AuditorTeam auditorTeam =  auditorTeamRepository.findById(auditorTeamId).orElseThrow(()-> new EntityNotFoundException("company"));
        if(!sessionUser.verifyPermissionOverCommercialEntity(auditorTeam ,onlyRead, absolutePermission)) throw  new UnauthorizedException(sessionUser.getUsername());
        return auditorTeam;
    }

    public AuditorTeamDTO getAuditorTeam(long auditorTeamId){
        return searchAuditorTeam(auditorTeamId, false,true).toDTO(true);
    }
}

