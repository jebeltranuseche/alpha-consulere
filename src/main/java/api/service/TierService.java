package api.service;

import api.adminpayload.TierDTO;
import api.adminpayload.TiersDTO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

@Service
public interface TierService {
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public TierDTO createTier(TierDTO tierDTO);
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public TierDTO patchTier(long id, TierDTO tierDTO);
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public TierDTO deleteTier(long id);
    public TierDTO getTier(long id);
    public TiersDTO getTiers();

}
