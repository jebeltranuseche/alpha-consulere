package api.service;

import api.adminpayload.ProcessDTO;
import api.adminpayload.ProcessesDTO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

@Service
public interface ProcessService {
    public ProcessesDTO getProcesses();

    public ProcessDTO createProcess(ProcessDTO processDTO);

    @PreAuthorize("hasRole('CLIENT') or hasRole('ROLE_ADMIN')")
    public ProcessDTO deleteProcess(long processId);

    public ProcessDTO patchProcess(long processId, ProcessDTO processDTO);

    public ProcessDTO getProcess(long processId);

    public ProcessesDTO getVisibleProcesses();
}
