package api.service;


import api.adminpayload.RequirementDTO;
import api.adminpayload.RequirementsDTO;
import api.exception.EntityNotFoundException;
import api.exception.EntityWithActiveAudit;
import api.exception.UnauthorizedException;
import api.exception.UserWithoutTierCapacityException;
import api.model.Company;
import api.model.Department;
import api.model.Process;
import api.model.Requirement;
import api.model.tier.BusinessEntity;
import api.model.user.User;
import api.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


@Service
public class RequirementServiceImpl implements  RequirementService {
    @Autowired
    UserService userService;
    @Autowired
    ProcessRepository processRepository;

    @Autowired
    RequirementRepository requirementRepository;

    @Autowired
    DepartmentRepository departmentRepository;

    @Autowired
    UserRepository userRepository;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public RequirementsDTO getRequirements(){
        return new RequirementsDTO(requirementRepository.findAll(), true);
    }

    @Transactional
    public RequirementDTO createRequirement(RequirementDTO requirementDTO){
        User sessionUser = userService.getSessionUser();
        Department department =  departmentRepository.findById(requirementDTO.getDepartmentId()).orElseThrow(()-> new EntityNotFoundException("department"));
        if(!sessionUser.verifyResponsibilityOverDepartment(department, false)) throw new UnauthorizedException(sessionUser.getUsername());
        if(!sessionUser.verifyDelegatedCreationAbility(BusinessEntity.REQUIREMENT)) throw new UserWithoutTierCapacityException(sessionUser.getUsername());
        Requirement filterRequirement = null;
        if(requirementDTO.getFilterRequirementId() != 0)filterRequirement =  requirementRepository.findById(requirementDTO.getFilterRequirementId()).orElseThrow(()-> new EntityNotFoundException("filter requirement"));
        Requirement requirement = new Requirement(requirementDTO.getName(),requirementDTO.getDescription(),filterRequirement, department );
        requirementRepository.save(requirement);
        if(requirementDTO.getProcessesToAdd() != null && requirementDTO.getProcessesToAdd().size() > 0)
           alterProcessesByRequirement(requirement,requirementDTO.getProcessesToAdd(), true);
        return requirement.toDTO(true);
    }

    @Transactional
    @PreAuthorize("hasRole('CLIENT') or hasRole('ROLE_ADMIN')")
    public RequirementDTO deleteRequirement(long requirementId){
        Requirement requirement =  searchRequirementToAlter(requirementId);
        requirementRepository.delete(requirement);
        return requirement.toDTO(true);
    }

    @Transactional
    public RequirementDTO patchRequirement(long requirementId, RequirementDTO requirementDTO){
        Requirement requirement = searchRequirement(requirementId);
        if(requirementDTO.getName() != null && !requirementDTO.getName().equals("")) requirement.setName(requirementDTO.getName());
        if(requirementDTO.getDescription() != null && !requirementDTO.getDescription().equals("")) requirement.setDescription(requirementDTO.getDescription());
        if(requirementDTO.getFilterRequirementId() != 0) requirement.setFilterRequirement(requirementRepository.findById(requirementDTO.getFilterRequirementId()).orElseThrow(()-> new EntityNotFoundException("filter requirement")));
        if(requirementDTO.getProcessesToAdd() != null && requirementDTO.getProcessesToAdd().size() > 0) alterProcessesByRequirement(requirement,requirementDTO.getProcessesToAdd(),true);
        if(requirementDTO.getProcessesToRemove() != null && requirementDTO.getProcessesToRemove().size() > 0) alterProcessesByRequirement(requirement,requirementDTO.getProcessesToRemove(),false);
        requirementRepository.save(requirement);
        return requirement.toDTO(true );
    }


    private Requirement searchRequirement(long requirementId){
        User sessionUser = userService.getSessionUser();
        Requirement requirement =  requirementRepository.findById(requirementId).orElseThrow(() -> new EntityNotFoundException("requirement"));
        if(!sessionUser.verifyResponsibilityOverDepartment(requirement.getDepartment(), false)) throw  new UnauthorizedException(sessionUser.getUsername());
        return requirement;
    }

    private Requirement searchRequirementToAlter(long requirementId){
        Requirement requirement = searchRequirement(requirementId);
        if(requirement.hasActiveAudit()) throw new EntityWithActiveAudit("department");
        return requirement;
    }

    public RequirementDTO getRequirement(long requirementId){
        return searchRequirement(requirementId).toDTO(true);
    }

    @Transactional
    private void  alterProcessesByRequirement(Requirement requirement, Set<Long> processesIds, boolean add){
        requirement.getDepartment().getProcesses().forEach(process -> {
            if(processesIds.contains(process.getId()))
                if(add){
                    process.addRequirements(Collections.singleton(requirement));
                    requirement.addProcesses(Collections.singleton(process));
                }
                 else {
                    process.removeRequirements(Collections.singleton(requirement));
                    requirement.removeProcesses(Collections.singleton(process));
                 }
                processRepository.save(process);
        });
    }

    public RequirementsDTO getVisibleRequirements(){
        User sessionUser = userService.getSessionUser();
        Set<Department> departments = new HashSet<>(sessionUser.getLeadedDepartments());
        Set<Company> companies = new HashSet<>(sessionUser.getLeadedCompanies());
        if(companies.size() > 0) companies.forEach(company -> departments.addAll(company.getDepartments()));
        if(sessionUser.hasRolesForCreation()) companies.addAll(sessionUser.getOwnedCompanies());
        companies.forEach(company -> departments.addAll(company.getDepartments()));
        Set<Requirement> requirements = new HashSet<>();
        departments.forEach(department -> requirements.addAll(department.getRequirements()));
        return  new RequirementsDTO(requirements, true);
    }
}
