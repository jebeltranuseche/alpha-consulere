package api.service;

import api.adminpayload.CompaniesDTO;
import api.adminpayload.CompanyDTO;
import api.adminpayload.DepartmentsDTO;
import api.adminpayload.UserDTO;
import api.exception.EntityNotFoundException;
import api.exception.UnauthorizedException;
import api.exception.UserWithoutTierCapacityException;
import api.model.Company;
import api.model.Department;
import api.model.tier.BusinessEntity;
import api.model.user.User;
import api.payload.CreateUserDTO;
import api.repository.CompanyRepository;
import api.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class CompanyServiceImpl implements  CompanyService {
    @Autowired
    UserService userService;
    @Autowired
    CompanyRepository companyRepository;

    @Autowired
    UserRepository userRepository;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public CompaniesDTO getCompanies(){
        return new CompaniesDTO(companyRepository.findAll(), true);
    }

    @Transactional
    @PreAuthorize("hasRole('ROLE_CLIENT') or hasRole('ROLE_ADMIN')")
    public CompanyDTO createCompany(CompanyDTO companyDTO){
        User sessionUser = userService.getSessionUser();
        if(!sessionUser.verifyCreationAbility(BusinessEntity.COMPANY)) throw new UserWithoutTierCapacityException(sessionUser.getUsername());
        User owner = (sessionUser.hasRole("ROLE_ADMIN") && companyDTO.getOwnerId() != 0)?  userRepository.findById(companyDTO.getOwnerId()).orElseThrow(()-> new EntityNotFoundException("owner")) : sessionUser;
        User leader = userRepository.findById(companyDTO.getLeaderId()).orElse(null);
        Company company = new Company(companyDTO.getName(),companyDTO.getBin(),owner, leader);
        companyRepository.save(company);
        return company.toDTO(true);
    }

    @Transactional
    @PreAuthorize("hasRole('CLIENT') or hasRole('ROLE_ADMIN')")
    public CompanyDTO deleteCompany(long companyId){
       Company company =  searchCompany(companyId,false , true);
       companyRepository.delete(company);
       return company.toDTO(true);
    }

    @Transactional
    public CompanyDTO patchCompany(long companyId, CompanyDTO companyDTO){
        Company company = searchCompany(companyId,false, companyDTO.getLeaderId() != 0);
        if(companyDTO.getName() != null && !companyDTO.getName().equals("")) company.setName(companyDTO.getName());
        if(companyDTO.getBin() != null && !companyDTO.getBin().equals("")) company.setBin(companyDTO.getBin());
        if(companyDTO.getLeaderId() != 0 && !companyDTO.isUnsetLeader() ){
            User leader = userRepository.findById(companyDTO.getLeaderId()).orElseThrow(()-> new EntityNotFoundException("leader"));
            company.setLeader(leader);
        }
        else if (companyDTO.isUnsetLeader()) company.setLeader(null);
        companyRepository.save(company);
        return company.toDTO(true);
    }

    private Company searchCompany(long companyId, boolean onlyRead,boolean absolutePermission){
        User sessionUser = userService.getSessionUser();
        Company company =  companyRepository.findById(companyId).orElseThrow(()-> new EntityNotFoundException("company"));
        if(!sessionUser.verifyPermissionOverCommercialEntity(company,onlyRead,absolutePermission)) throw  new UnauthorizedException(sessionUser.getUsername());
        return company;
    }

    public CompanyDTO getCompany(long companyId){
        return searchCompany(companyId, true ,false).toDTO(true);
    }


    public CompaniesDTO getVisibleCompanies(){
        User sessionUser = userService.getSessionUser();
        if(!sessionUser.isUserActive()) throw  new UnauthorizedException(sessionUser.getUsername());
        Set<Company> companies = getCompaniesOfLeadedDepartments(sessionUser);
        companies.addAll(sessionUser.getLeadedCompanies());
        if(sessionUser.hasRolesForCreation()) companies.addAll(sessionUser.getOwnedCompanies());
        return new CompaniesDTO(companies, true);
    }

    private  Set<Company> getCompaniesOfLeadedDepartments(User user){
        Set<Company> companies =  new HashSet<>();
        user.getLeadedDepartments().forEach(department -> companies.add(department.getCompany()));
        return companies;
    }

    public DepartmentsDTO getCompanyDepartments(long companyId){
       User sessionUser = userService.getSessionUser();
       Company company = companyRepository.findById(companyId).orElseThrow(() -> new EntityNotFoundException("company"));
       Set<Department>  departments = new HashSet<>();
       if(sessionUser.verifyResponsibilityOfCommercialEntity(company)) departments.addAll(company.getDepartments());
       else departments.addAll(company.getDepartments().stream().filter(department -> department.getLeader() != null && department.getLeader().getId() == sessionUser.getId()).collect(Collectors.toSet()));
       return new DepartmentsDTO(departments, true);
    }
}
