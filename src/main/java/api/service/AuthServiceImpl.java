package api.service;

import api.adminpayload.UserDTO;
import api.exception.AppException;
import api.exception.BadRequestException;
import api.exception.EntityNotFoundException;
import api.model.user.User;
import api.model.role_based_access.Role;
import api.payload.JwtAuthenticationResponse;
import api.payload.LoginRequest;
import api.payload.BasicUserDTO;
import api.repository.RoleRepository;
import api.repository.UserRepository;
import api.security.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.Collection;
import java.util.Collections;

@Service
public class AuthServiceImpl implements AuthService {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtTokenProvider tokenProvider;

    @Autowired
    UserService userService;


    @Override
    public JwtAuthenticationResponse authenticateUser(@Valid LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsernameOrEmail(),
                        loginRequest.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = tokenProvider.generateToken(authentication);
        return new JwtAuthenticationResponse(jwt, "Bearer", authentication.getName(), (Collection<GrantedAuthority>) authentication.getAuthorities(), getSessionUser("auth").getId());
    }

    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_CLIENT')")
    public UserDTO registerUser(@Valid @RequestBody BasicUserDTO basicUserDTO) {
        userService.validateUserAvailability(basicUserDTO.getUsername(), basicUserDTO.getEmail());
        if(basicUserDTO.getProfile() == null) throw  new BadRequestException("User needs profile to be created");
        User user = new User(basicUserDTO.getUsername(), basicUserDTO.getEmail(), basicUserDTO.getPassword());
        Role userRole = roleRepository.findByName("ROLE_USER").orElseThrow(() -> new AppException("User Role not found"));
        user.setRoles(Collections.singleton(userRole));
        user.setOwner(getSessionUser("Owner"));
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setProfile(basicUserDTO.getProfile());
        User result = userRepository.save(user);
        UserDTO resultDTO= new UserDTO();
        resultDTO.setForBasicUserInfo(result);
        return resultDTO;
    }

    @Override
    public Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    public User getSessionUser(String contextEntity){
        return userRepository.findByUsername(getAuthentication().getName()).orElseThrow(() -> new EntityNotFoundException(contextEntity));
    }

}
