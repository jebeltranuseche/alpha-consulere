package api.service;

import api.adminpayload.AuditorTeamDTO;
import api.adminpayload.AuditorTeamsDTO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

@Service
public interface AuditorTeamService {
    public AuditorTeamsDTO getAuditorTeams();

    @PreAuthorize("hasRole('ROLE_CLIENT') or hasRole('ROLE_ADMIN')")
    public AuditorTeamDTO createAuditorTeam(AuditorTeamDTO auditorTeamDTO);

    @PreAuthorize("hasRole('CLIENT') or hasRole('ROLE_ADMIN')")
    public AuditorTeamDTO deleteAuditorTeam(long auditorTeamId);

    public AuditorTeamDTO patchAuditorTeam(long auditorTeamId, AuditorTeamDTO auditorTeamDTO);

    public AuditorTeamDTO getAuditorTeam(long auditorTeamId);
}
