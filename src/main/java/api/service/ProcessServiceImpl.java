package api.service;

import api.adminpayload.ProcessDTO;
import api.adminpayload.DepartmentsDTO;
import api.adminpayload.ProcessesDTO;
import api.adminpayload.RequirementsDTO;
import api.exception.EntityNotFoundException;
import api.exception.EntityWithActiveAudit;
import api.exception.UnauthorizedException;
import api.exception.UserWithoutTierCapacityException;
import api.model.Company;
import api.model.Department;
import api.model.Process;
import api.model.Requirement;
import api.model.tier.BusinessEntity;
import api.model.user.User;
import api.repository.DepartmentRepository;
import api.repository.ProcessRepository;
import api.repository.RequirementRepository;
import api.repository.UserRepository;
import org.apache.tomcat.jni.Proc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

@Service
public class ProcessServiceImpl implements ProcessService{
    @Autowired
    UserService userService;

    @Autowired
    DepartmentRepository departmentRepository;

    @Autowired
    RequirementRepository requirementRepository;

    @Autowired
    ProcessRepository processRepository;

    @Autowired
    UserRepository userRepository;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ProcessesDTO getProcesses(){
        return new ProcessesDTO(processRepository.findAll(), true);
    }

    @Transactional
    public ProcessDTO createProcess(ProcessDTO processDTO){
        User sessionUser = userService.getSessionUser();
        Department department = departmentRepository.findById(processDTO.getDepartmentId()).orElseThrow(()-> new EntityNotFoundException("department") );
        if(!sessionUser.verifyResponsibilityOverDepartment(department, false)) throw new UnauthorizedException(sessionUser.getUsername());
        if(!sessionUser.verifyDelegatedCreationAbility(BusinessEntity.PROCESS)) throw new UserWithoutTierCapacityException(sessionUser.getUsername());
        Process process =  new Process(processDTO.getName(),processDTO.getInput(),processDTO.getOutput(), department);
        if(processDTO.getRequirementsToAdd() != null && processDTO.getRequirementsToAdd().size() != 0) process.addRequirements(findRequirementsToAlter(department,processDTO.getRequirementsToAdd()));
        processRepository.save(process);
        return process.toDTO(true);
    }

    @Transactional
    @PreAuthorize("hasRole('CLIENT') or hasRole('ROLE_ADMIN')")
    public ProcessDTO deleteProcess(long processId){
        Process process =  searchProcessToAlter(processId);
        processRepository.delete(process);
        return process.toDTO(true);
    }

    @Transactional
    public ProcessDTO patchProcess(long processId, ProcessDTO processDTO){
        Process process = searchProcessToAlter(processId);
        if(processDTO.getName() != null && !processDTO.getName().equals("")) process.setName(processDTO.getName());
        if(processDTO.getInput() != null && !processDTO.getInput().equals("")) process.setInput(processDTO.getInput());
        if(processDTO.getOutput() != null && !processDTO.getOutput().equals("")) process.setOutput(processDTO.getOutput());
        if(processDTO.getRequirementsToAdd() != null && processDTO.getRequirementsToAdd().size() != 0) process.addRequirements(findRequirementsToAlter(process.getDepartment(),processDTO.getRequirementsToAdd()));
        if(processDTO.getRequirementsToRemove() != null && processDTO.getRequirementsToRemove().size() != 0) process.removeRequirements(findRequirementsToAlter(process.getDepartment(), processDTO.getRequirementsToRemove()));
        processRepository.save(process);
        return process.toDTO(true);
    }

    private Process searchProcess(long processId){
        User sessionUser = userService.getSessionUser();
        Process process =  processRepository.findById(processId).orElseThrow(() -> new EntityNotFoundException("process"));
        Department department = process.getDepartment();
        Company company = department.getCompany();
        if(!sessionUser.verifyResponsibilityOverDepartment(process.getDepartment(), false)) throw  new UnauthorizedException(sessionUser.getUsername());
        return process;
    }

    private Process searchProcessToAlter(long processId){
        Process process = searchProcess(processId);
        if(process.hasActiveAudit()) throw new EntityWithActiveAudit("Process");
        return process;
    }

    public ProcessDTO getProcess(long processId){
        return searchProcess(processId).toDTO(true);
    }

    private Set<Requirement> findRequirementsToAlter(Department department, Set<Long> requirementsIds){
        Set<Requirement> requirements = new HashSet<>();
        department.getRequirements().forEach(requirement -> {if(requirementsIds.contains(requirement.getId())) requirements.add(requirement);} );
        return  requirements;
    }

    public ProcessesDTO getVisibleProcesses(){
        User sessionUser = userService.getSessionUser();
        Set<Department> departments = new HashSet<>(sessionUser.getLeadedDepartments());
        Set<Company> companies = new HashSet<>(sessionUser.getLeadedCompanies());
        if(companies.size() > 0) companies.forEach(company -> departments.addAll(company.getDepartments()));
        if(sessionUser.hasRolesForCreation()) companies.addAll(sessionUser.getOwnedCompanies());
        companies.forEach(company -> departments.addAll(company.getDepartments()));
        Set<Process> processes = new HashSet<>();
        departments.forEach(department -> processes.addAll(department.getProcesses()));
        return  new ProcessesDTO(processes, true);
    }
}
