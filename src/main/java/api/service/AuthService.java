package api.service;

import api.adminpayload.UserDTO;
import api.payload.JwtAuthenticationResponse;
import api.payload.LoginRequest;
import api.payload.BasicUserDTO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

@Service
public interface AuthService {
    public JwtAuthenticationResponse authenticateUser(@Valid @RequestBody LoginRequest loginRequest);

    @PreAuthorize("hasRole('CLIENT') or hasRole('ROLE_ADMIN')")
    public UserDTO registerUser(@Valid @RequestBody BasicUserDTO basicUserDTO);

    public Authentication getAuthentication();

}
