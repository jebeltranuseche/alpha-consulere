package api.service;

import api.adminpayload.TierDTO;
import api.adminpayload.TiersDTO;
import api.exception.EntityNotFoundException;
import api.exception.TierException;
import api.model.tier.Tier;
import api.repository.TierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.Set;

@Service
public class TierServiceImpl implements TierService {
    @Autowired
    TierRepository tierRepository;

    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @Transactional
    public TierDTO createTier(TierDTO tierDTO) {
        checkTierNameAvailability(tierDTO.getName());
        Tier tier = new Tier(tierDTO.getName());
        tier.addOrUpdateTierCapacities(tierDTO.getTierCapacities());
        tierRepository.save(tier);
        return new TierDTO(tier);
    }

    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @Transactional
    public TierDTO patchTier(long id, TierDTO tierDTO) {
        Tier tier = getTierOrFail(id);
       if(tierDTO.getName() != null){
           checkTierNameAvailability(tierDTO.getName());
           tier.setName(tierDTO.getName());
       }
       if(tierDTO.getTierCapacities() != null && tierDTO.getTierCapacities().size()>0)
           tier.addOrUpdateTierCapacities(tierDTO.getTierCapacities());
       tier.getTierCapacities().forEach(tierCapacity -> System.out.println(tierCapacity.getEntity() + ": "+ tierCapacity.getCapacity()));
       tierRepository.save(tier);
       return new TierDTO(tier);
    }

    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @Transactional
    public TierDTO deleteTier(long id) {
        Tier tier = getTierOrFail(id);
        tierRepository.delete(tier);
        return new TierDTO(tier);
    }

    @Override
    public TierDTO getTier(long id) {
        Tier tier = getTierOrFail(id);
        return new TierDTO(tier);
    }

    @Override
    public TiersDTO getTiers() {
        Set<Tier> tiers = tierRepository.findAll();
        return new TiersDTO(tiers);
    }

    private Tier getTierOrFail(long id){
        return tierRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("tier"));
    }

    private boolean checkTierNameAvailability(String name){
        Tier tier = tierRepository.findByName(name).orElse(null);
        if(tier != null) throw new TierException("Tier with name already exists");
        return true;
    }
}
