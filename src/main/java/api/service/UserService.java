package api.service;

import api.adminpayload.ContractTierDTO;
import api.adminpayload.UserCapacityDTO;
import api.adminpayload.UserDTO;
import api.adminpayload.UsersDTO;
import api.model.user.User;
import api.payload.ContractTierRequest;
import api.payload.CreateUserDTO;
import api.payload.SoftPatchUserDTO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public interface UserService {
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public UsersDTO getUsers();
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public UserDTO createUser(CreateUserDTO createUserDTO);

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ContractTierDTO affiliateUser(long userId, ContractTierRequest contractTierRequest);

    public UsersDTO getOwnedUsers();
    @PreAuthorize("hasRole('CLIENT') or hasRole('ROLE_ADMIN')")
    public UserDTO deleteUser(long id);

    @PreAuthorize("hasRole('CLIENT') or hasRole('ROLE_ADMIN')")
    public UserCapacityDTO getSessionUserActualCapacityFor(String entity);

    public Set<UserCapacityDTO> loadAllActualCapacities();

    public UserDTO patchUser(long userId, SoftPatchUserDTO softPatchUserDTO);

    public boolean validateUserAvailability(String username, String email);

    public boolean validateUsernameAvailability(String username);

    public boolean validateEmailAvailability(String email);

    public UserDTO getUser(long userId);

    public UserDTO getSession();

    public User getSessionUser();

}
