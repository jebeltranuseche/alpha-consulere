package api.service;


import api.adminpayload.CompaniesDTO;
import api.adminpayload.DepartmentDTO;
import api.adminpayload.DepartmentsDTO;
import api.exception.EntityNotFoundException;
import api.exception.EntityWithActiveAudit;
import api.exception.UnauthorizedException;
import api.exception.UserWithoutTierCapacityException;
import api.model.Company;
import api.model.Department;
import api.model.tier.BusinessEntity;
import api.model.user.User;
import api.repository.CompanyRepository;
import api.repository.DepartmentRepository;
import api.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

@Service
public class DepartmentServiceImpl implements  DepartmentService {
    @Autowired
    UserService userService;

    @Autowired
    CompanyService companyService;

    @Autowired
    DepartmentRepository departmentRepository;

    @Autowired
    CompanyRepository companyRepository;

    @Autowired
    UserRepository userRepository;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public DepartmentsDTO getDepartments(){
        return new DepartmentsDTO(departmentRepository.findAll(), true);
    }

    @Transactional
    @PreAuthorize("hasRole('ROLE_CLIENT') or hasRole('ROLE_ADMIN')")
    public DepartmentDTO createDepartment(DepartmentDTO departmentDTO){
        User sessionUser = userService.getSessionUser();
        if(!sessionUser.verifyCreationAbility(BusinessEntity.DEPARTMENT)) throw new UserWithoutTierCapacityException(sessionUser.getUsername());
        User leader = userRepository.findById(departmentDTO.getLeaderId()).orElse(null);
        Company company =  companyRepository.findById(departmentDTO.getCompanyId()).orElseThrow(()-> new EntityNotFoundException("Company"));
        if(!sessionUser.verifyResponsibilityOverCompany(company,false)) throw  new UnauthorizedException(sessionUser.getUsername()
        );
        Department department = new Department(departmentDTO.getName(), leader, company);
        departmentRepository.save(department);
        return department.toDTO(true);
    }

    @Transactional
    @PreAuthorize("hasRole('CLIENT') or hasRole('ROLE_ADMIN')")
    public DepartmentDTO deleteDepartment(long departmentId){
        Department department =  searchDepartmentToAlter(departmentId, true);
        departmentRepository.delete(department);
        return department.toDTO(true);
    }

    @Transactional
    public DepartmentDTO patchDepartment(long departmentId, DepartmentDTO departmentDTO){
        Department department = searchDepartmentToAlter(departmentId, departmentDTO.getLeaderId() != 0);
        if(departmentDTO.getName() != null && !departmentDTO.getName().equals("")) department.setName(departmentDTO.getName());
        if(departmentDTO.getLeaderId() != 0 && !departmentDTO.isUnsetLeader()){
            User leader = userRepository.findById(departmentDTO.getLeaderId()).orElseThrow(()-> new EntityNotFoundException("leader"));
            department.setLeader(leader);
        }
        else if (departmentDTO.isUnsetLeader()) department.setLeader(null);
        departmentRepository.save(department);
        return department.toDTO(true);
    }

    private Department searchDepartment(long departmentId, boolean absolutePermission){
        User sessionUser = userService.getSessionUser();
        Department department =  departmentRepository.findById(departmentId).orElseThrow(() -> new EntityNotFoundException("department"));
        if(!sessionUser.verifyResponsibilityOverDepartment(department, absolutePermission)) throw  new UnauthorizedException(sessionUser.getUsername());
        return department;
    }

    private Department searchDepartmentToAlter(long departmentId, boolean absolutePermission){
        Department department = searchDepartment(departmentId , absolutePermission);
        if(department.hasActiveAudit()) throw new EntityWithActiveAudit("department");
        return department;
    }

    public DepartmentDTO getDepartment(long departmentId){
        return searchDepartment(departmentId,false).toDTO(true);
    }

    public DepartmentsDTO getVisibleDepartments(){
        User sessionUser = userService.getSessionUser();
        Set<Department> departments = new HashSet<>(sessionUser.getLeadedDepartments());
        Set<Company> companies = new HashSet<>(sessionUser.getLeadedCompanies());
        if(companies.size() > 0) companies.forEach(company -> departments.addAll(company.getDepartments()));
        if(sessionUser.hasRolesForCreation()) companies.addAll(sessionUser.getOwnedCompanies());
        companies.forEach(company -> departments.addAll(company.getDepartments()));
        return  new DepartmentsDTO(departments, true);
    }
}
