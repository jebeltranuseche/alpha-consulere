package api.service;

import api.adminpayload.*;
import api.payload.CreateUserDTO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

@Service
public interface CompanyService {

        @PreAuthorize("hasRole('ROLE_ADMIN')")
        public CompaniesDTO getCompanies();

        @PreAuthorize("hasRole('ROLE_CLIENT') or hasRole('ROLE_ADMIN')")
        public CompanyDTO createCompany(CompanyDTO companyDTO);

        @PreAuthorize("hasRole('CLIENT') or hasRole('ROLE_ADMIN')")
        public CompanyDTO deleteCompany(long companyId);

        public CompanyDTO patchCompany(long companyId, CompanyDTO companyDTO);

        public CompanyDTO  getCompany(long companyId);

        public CompaniesDTO getVisibleCompanies();

        public DepartmentsDTO getCompanyDepartments(long companyId);

    }
