package api.repository;

import api.model.Process;
import api.model.Requirement;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Set;

public interface ProcessRepository extends PagingAndSortingRepository<Process, Long> {
    public Set<Process> findAll();
    public Set<Process> findById(List<Long> ids);
    public Set<Process> findByName(Set<String> names);
}
