package api.repository;

import api.model.tier.Tier;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;
import java.util.Set;

public interface TierRepository  extends PagingAndSortingRepository<Tier, Long> {
    Optional<Tier> findByName(String name);
    Set<Tier> findByIdIn(long[] rolesIds);
    Set<Tier> findAll();
}
