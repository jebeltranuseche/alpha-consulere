package api.repository;

import api.model.Company;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Set;

public interface CompanyRepository  extends PagingAndSortingRepository<Company, Long> {
   public  Set<Company> findAll();
}
