package api.repository;

import api.model.Requirement;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Set;

public interface RequirementRepository extends PagingAndSortingRepository<Requirement, Long> {
    public Set<Requirement> findAll();
}