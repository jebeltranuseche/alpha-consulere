package api.repository;

import api.model.Department;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Set;

public interface DepartmentRepository  extends PagingAndSortingRepository<Department, Long> {
    public Set<Department> findAll();

}
