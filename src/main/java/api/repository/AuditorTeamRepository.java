package api.repository;

import api.model.AuditorTeam;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Set;

public interface AuditorTeamRepository extends PagingAndSortingRepository<AuditorTeam, Long> {
    public Set<AuditorTeam> findAll();
}
