package api.repository;

import api.model.AuditRequirement;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Set;

public interface AuditRequirementRepository extends PagingAndSortingRepository<AuditRequirement, Long> {
    public Set<AuditRequirement> findAll();
}