package api.repository;

import api.model.user.User;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Long> {
    Optional<User> findByEmail(String email);

    Optional<User> findByUsernameOrEmail(String username, String email);

    Set<User> findByIdIn( long[] usersIds);

    Set<User> findAll( );

    public Set<User> findAllById(long[] ids);

    long deleteById(long id);

    long deleteByIdIn(Set<Long> ids);

    Optional<User> findByUsername(String username);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);
}