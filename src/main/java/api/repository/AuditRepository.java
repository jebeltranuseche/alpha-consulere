package api.repository;

import api.model.Audit;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Set;

public interface AuditRepository extends PagingAndSortingRepository<Audit, Long> {
    public Set<Audit> findAll();
}
