package api.repository;

import api.model.Audit;
import api.model.NonConformity;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Set;

public interface NonConformityRepository extends PagingAndSortingRepository<NonConformity, Long> {
    public Set<NonConformity> findAll();
    public Set<NonConformity> findAllById(long[] ids);
}